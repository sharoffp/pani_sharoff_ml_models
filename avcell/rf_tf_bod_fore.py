
from __future__ import print_function
 import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy import array

from statsmodels.graphics.tsaplots import plot_pacf
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance
import pickle

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

import tensorflow as tf
from tensorflow.python.ops import resources
from tensorflow.contrib.tensor_forest.python import tensor_forest

# Ignore all GPUs, tf random forest does not benefit from it.
import os
os.environ["CUDA_VISIBLE_DEVICES"] = ""


def class_metrics(y_true, y_pred):
    y_test_cat = y_true>5.5
    y_pred_cat = y_pred>5.5
    tn, fp, fn, tp = confusion_matrix(y_test_cat, y_pred_cat).ravel()
    recall = tp/(tp+fn)
    print("true positive", tp)
    print("total positive events", np.sum(y_test_cat))
    print("recall", recall)
    precision = tp/(tp+fp)
    print("total postive events in ypred", np.sum(y_pred_cat))
    print("precision", precision)


with open('y.pickle', 'rb') as f:
    pickle_y = pickle.load(f)


with open('y.pickle', 'rb') as f:
    pickle_x = pickle.load(f)


X_train, X_test, y_train, y_test = train_test_split(pickle_x,pickle_y, test_size=0.2, random_state=42)


y_train_cat = y_train>5.5
y_test_cat = y_test>5.5

batch_size = 64
SHUFFLE_BUFFER_SIZE = 100

train_examples = X_train
train_labels =  y_train_cat
test_examples = X_test
test_labels = y_test_cat

tf.convert_to_tensor(data_np, np.float32)

train_dataset = tf.data.Dataset.from_tensor_slices((train_examples, train_labels))
test_dataset = tf.data.Dataset.from_tensor_slices((test_examples, test_labels))

train_dataset = train_dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(batch_size)
test_dataset = test_dataset.batch(batch_size)


num_steps = 500 # Total steps to train
batch_size = 64 # The number of samples per batch
num_classes = 2 # The 10 digits
num_features = 80 # Each image is 28x28 pixels
num_trees = 10
max_nodes = 1000

# Input and Target data
X = tf.placeholder(tf.float32, shape=[None, num_features])
# For random forest, labels must be integers (the class id)
Y = tf.placeholder(tf.int32, shape=[None])

# Random Forest Parameters
hparams = tensor_forest.ForestHParams(num_classes=num_classes, num_features=num_features, num_trees=num_trees, max_nodes=max_nodes).fill()


# Build the Random Forest
forest_graph = tensor_forest.RandomForestGraphs(hparams)
# Get training graph and loss
train_op = forest_graph.training_graph(X, Y)
loss_op = forest_graph.training_loss(X, Y)

# Measure the accuracy
infer_op, _, _ = forest_graph.inference_graph(X)
correct_prediction = tf.equal(tf.argmax(infer_op, 1), tf.cast(Y, tf.int64))
accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Initialize the variables (i.e. assign their default value) and forest resources
init_vars = tf.group(tf.global_variables_initializer(),resources.initialize_resources(resources.shared_resources()))

# Start TensorFlow session
sess = tf.train.MonitoredSession()

# Run the initializer
sess.run(init_vars)

# Training
for i in range(1, num_steps + 1):
    # Prepare Data
    batch_x, batch_y = train_dataset.batch(batch_size)
    print(type(batch_x))
    _, l = sess.run([train_op, loss_op], feed_dict={X: batch_x, Y: batch_y})
    if i % 50 == 0 or i == 1:
        acc = sess.run(accuracy_op, feed_dict={X: batch_x, Y: batch_y})
        print('Step %i, Loss: %f, Acc: %f' % (i, l, acc))
