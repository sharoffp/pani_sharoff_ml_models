#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import pickle

rng = numpy.random

# Parameters
learning_rate = 0.01
training_epochs = 1000
display_step = 50


with open('y_data.pickle', 'rb') as f:
    # The protocol version used is detected automatically, so we do not
    # have to specify it.
    pickle_y = pickle.load(f)


with open('x_data.pickle', 'rb') as f:
    # The protocol version used is detected automatically, so we do not
    # have to specify it.
    pickle_x = pickle.load(f)




# from data_clean import data_cleaning
# pickle_x, pickle_y = data_cleaning()


pickle_x = pickle_x[:,:80]

pickle_y = pickle_y.reshape((-1,1))


print(pickle_x.dtype)
print(pickle_y.dtype)
# pickle_x = numpy.float64(pickle_x)
# pickle_y = numpy.float64(pickle_y)

from sklearn.model_selection import train_test_split
train_X,test_X, train_Y, test_Y = train_test_split(pickle_x,pickle_y, test_size=0.2, random_state=42)


# y_train_cat = y_train>5.5
# y_test_cat = y_test>5.5

batch_size = 64
SHUFFLE_BUFFER_SIZE = 100


# Data

n_samples = train_X.shape[0]
row = train_X.shape[0]
column = train_X.shape[1]

print(row, column)

# tf Graph Input
X = tf.placeholder(tf.float32, [None, column])
Y = tf.placeholder(tf.float32, [None,1])

n_samples = train_X.shape[0]

# Parameters
learning_rate = 0.1
num_steps = 500
training_epochs = 100
batch_size = 128
display_step = 10

# Network Parameters
n_hidden_1 = 256 # 1st layer number of neurons
n_hidden_2 = 256 # 2nd layer number of neurons
num_classes = 1 # since we are predicting one value


# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([column, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, num_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([num_classes]))
}

# Create model
def neural_net(x):
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer_1 = tf.nn.relu(layer_1)
    # Hidden fully connected layer 
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    layer_2 = tf.nn.relu(layer_2)
    # Output fully connected layer with a neuron for each class
    out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
    return out_layer

# Construct model
logits = neural_net(X)

# Define loss and optimizer

# Mean squared error
cost = tf.reduce_sum(tf.pow(logits-Y, 2))/(2*n_samples)


# loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(cost)

# # Evaluate model (with test logits, for dropout to be disabled)
# correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(Y, 1))
# accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()


# Start training
with tf.Session() as sess:

    # Run the initializer
    sess.run(init)

    for epoch in range(training_epochs):
        for (x, y) in zip(train_X, train_Y):
            x = x.reshape((1,-1))
            y = y.reshape((1,-1))
        # Run optimization op (backprop)
            sess.run(train_op, feed_dict={X: x, Y: y})
        if epoch % display_step == 0 or epoch == 1:
            # Calculate batch loss and accuracy
            loss = sess.run([cost], feed_dict={X: x, Y: y})
            print("Step " + str(epoch) + ", Minibatch Loss=  " ,loss)














