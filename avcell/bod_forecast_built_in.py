#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from numpy import array

from statsmodels.graphics.tsaplots import plot_pacf
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
# from sklearn.linear_model import LogisticRegression


def class_metrics(y_true, y_pred):
    y_test_cat = y_true>5.5
    y_pred_cat = y_pred>5.5
    tn, fp, fn, tp = confusion_matrix(y_test_cat, y_pred_cat).ravel()
    recall = tp/(tp+fn)
    print("true positive", tp)
    print("total positive events", np.sum(y_test_cat))
    print("recall", recall)
    precision = tp/(tp+fp)
    print("total postive events in ypred", np.sum(y_pred_cat))
    print("precision", precision)
    return [tn, fp, fn, tp]




excel_data_df = pd.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')



#Filtering the data 
excel_data_df = excel_data_df.iloc[7:,:]
# excel_data_df = excel_data_df.fillna(method='ffill')



#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df = excel_data_df.drop(excel_data_df.columns[[remove_columns]], axis=1)

excel_data_df = excel_data_df.iloc[3:,:]

rem_cols = [37,70]
excel_data_df = excel_data_df.drop(excel_data_df.columns[[rem_cols]], axis=1)
print("sum of nans", excel_data_df.isnull().sum())


FE_BOD_LOADING_NAN = excel_data_df[excel_data_df['FE BOD LOADING'].isnull()]
FE_BOD_LOADING_index = FE_BOD_LOADING_NAN.index
FE_BOD_LOADING_index_array = np.array(FE_BOD_LOADING_index)

#unwanted strings available
excel_data_df.iloc[780,14] = excel_data_df.iloc[781,14]
excel_data_df.iloc[780,24] = excel_data_df.iloc[781,24]

excel_data_df_float = excel_data_df.astype('float64', copy = True, errors='raise')

excel_data_df_float = excel_data_df_float.interpolate(method='spline',order = 2, axis = 0)
print("checking if any other nans", excel_data_df_float.isnull().sum())

excel_data_df_float = excel_data_df_float.drop(FE_BOD_LOADING_index_array)

excel_data_df = excel_data_df_float

#droping pandas columns

y = excel_data_df["FE BOD LOADING"]

#extracting the target variable
y = y.values

# df.drop(['B', 'C'], axis=1)

excel_data_df = excel_data_df.drop(["BOD LOSSES", "FE BOD LOADING", "PCO COD:BOD RATIO", "FE COD:BOD RATIO", "PCO COD:BOD RATIO.1", "FE COD:BOD RATIO.1", "ACTUAL PCO BOD", "ACTUAL FE BOD"], axis = 1)

# "TSS DAILY LIMIT"

excel_data_df = excel_data_df.drop(["BOD DAILY LIMIT", "BOD MONTHLY LIMIT" , "TSS MONTHLY LIMIT", "Unnamed: 148", "Unnamed: 150", "Unnamed: 151", "Unnamed: 152", "Unnamed: 156", "Unnamed: 157", "Unnamed: 159"], axis =1)

excel_data_df = excel_data_df.drop(["FE BOD"], axis =1)

excel_data_df = excel_data_df.drop(["FE BOD 7-d roll. Avg", "DISCHARGE kg of BOD/MT", "DISCHARGE kg of BOD/MT.1"], axis =1)

excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.2", "PCO LOADING kg of BOD/MT" , "PCO BOD.1"], axis =1)

excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.1"], axis =1)

excel_data_df = excel_data_df.drop(["PCO BOD"], axis =1)


excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

excel_data_df = excel_data_df.drop(["Carbon to Plant", "Carbon used by bugs", "CARBON: NITROGEN RATIO"],axis =1)
excel_data_df = excel_data_df.drop([ "CARBON:PHOS RATIO", "BOD", "BOD.1", "TSS.1", "TSS.2", "TSS", "DISCHARGE kg of TSS/MT", "DISCHARGE kg of TSS/MT.1", "BOD.3", "PCO BOD.2"], axis =1)

# excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

excel_data_df = excel_data_df.drop([' TSS DAILY LIMIT'], axis =1)


excel_data_df = excel_data_df.drop(['Unnamed: 149', 'Unnamed: 160', 'Unnamed: 161'], axis =1)

excel_data_df = excel_data_df.drop(['NITROGEN:PHOS RATIO'], axis =1)


#limits
excel_data_df = excel_data_df.drop(['Unnamed: 80'], axis =1)

excel_data_df = excel_data_df.drop(['Unnamed: 76'], axis =1)

excel_data_df = excel_data_df.drop(['Unnamed: 82'], axis =1)

excel_data_df = excel_data_df.drop(['Unnamed: 86'], axis =1)


excel_data_df = excel_data_df.drop(['Unnamed: 153', 'Unnamed: 154'], axis =1)


# excel_data_df = excel_data_df.drop(['FE COD.2'], axis =1)
excel_data_df = excel_data_df.drop(['Unnamed: 74', 'Unnamed: 84'], axis =1)

excel_data_df = excel_data_df.drop(['BOD.2'], axis =1)


# excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

# "PCO LOADING OF BOD/MT.1"
#renaming features
excel_data_df = excel_data_df.rename(columns = {"Unnamed: 1": "Effluent Flow", "Unnamed: 144": "PCO COD 7-d roll avg"})


# excel_data_df['NITROGEN:PHOS RATIO'] = excel_data_df_float['NITROGEN:PHOS RATIO']

#one hot encoding
# Nitrogen_phos_ratio = pd.get_dummies(excel_data_df['NITROGEN:PHOS RATIO'], prefix = 'NITROGEN:PHOS RATIO')

#removing this because mostly 20 and only few values (unstable)
# excel_data_df = excel_data_df.drop(['NITROGEN:PHOS RATIO'], axis =1)


feature_list = excel_data_df.columns.tolist()
feature_array = np.array(feature_list)


x = excel_data_df.values
#to remove one nan values on top
x = x[1:,:]
y = y[1:]


X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)


nan_index = np.isnan(x)
np.where(nan_index)


#Linear regression:
model_lr = LinearRegression()
model_lr_fit = model_lr.fit(X_train, y_train)
y_pred_lr = model_lr_fit.predict(X_test)


mae_lr = mean_absolute_error(y_test, y_pred_lr)

print("mae for lr", mae_lr)

class_metrics(y_test, y_pred_lr)


#plotting
plt.plot(y_test[0:30])
plt.plot(y_pred_lr[0:30])


from sklearn import linear_model
reg_ls = linear_model.Lasso(alpha=0.2)
model_ls_fit = reg_ls.fit(X_train, y_train)
y_pred_ls = model_ls_fit.predict(X_test)

mae_lasso = mean_absolute_error(y_test, y_pred_ls)

print("mae for ls", mae_lasso)

class_metrics(y_test, y_pred_ls)

#plotting
plt.plot(y_test[0:30])
plt.plot(y_pred_ls[0:30])



#Random forest

from sklearn.ensemble import RandomForestRegressor
model_rfr = RandomForestRegressor(max_depth=10, random_state=0)
model_rfr.fit(X_train, y_train)
y_pred_rfr = model_rfr.predict(X_test)

mae_rfr = mean_absolute_error(y_test, y_pred_rfr)

print("mae for rf: ", mae_rfr)

class_metrics(y_test, y_pred_rfr)

plt.plot(y_test[30:60])
plt.plot(y_pred_rfr[30:60])


from sklearn.inspection import permutation_importance

# perform permutation importance
results = permutation_importance(model_rfr, X_test, y_test,n_repeats = 30, random_state = 1, scoring="neg_mean_absolute_error")
# get importance
importance_perm = results.importances_mean
# summarize feature importance
for i,v in enumerate(importance_perm):
    print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance
plt.figure(2)
plt.bar([x for x in range(len(importance_perm))], importance_perm, tick_label = np.arange(x.shape[1]))
plt.xticks(rotation=90)
plt.show()


#===Polynomial basis===

#Linear Regression
polynomial_features= PolynomialFeatures(degree=2)
X_train_poly = polynomial_features.fit_transform(X_train)
X_test_poly = polynomial_features.fit_transform(X_test)


model_poly_lr = LinearRegression()
model_poly_lr.fit(X_train_poly, y_train)
y_pred_poly_lr = model_poly_lr.predict(X_test_poly)


mae_poly2 = mean_absolute_error(y_test, y_pred_poly_lr)

print("mae for poly regression", mae_poly2)

plt.plot(y_test[30:60])
plt.plot(y_pred_poly_lr[30:60])

class_metrics(y_test, y_pred_poly_lr)


#Lasso regression
model_poly_ls = linear_model.Lasso(alpha=0.5)
model_poly_ls.fit(X_train_poly, y_train)
y_pred_poly_ls = model_poly_ls.predict(X_test_poly)


mae_poly2_ls = mean_absolute_error(y_test, y_pred_poly_ls)

print("mae for poly lasso regression", mae_poly2_ls)

plt.plot(y_test[0:60])
plt.plot(y_pred_poly_ls[0:60])


class_metrics(y_test, y_pred_poly_ls)


#Random forest

from sklearn.ensemble import RandomForestRegressor
model_rfr = RandomForestRegressor(max_depth=2, random_state=0)
model_rfr.fit(X_train, y_train)
y_pred_rfr = model_rfr.predict(X_test)

mae_rfr = mean_absolute_error(y_test, y_pred_rfr)




#=====================

# pca = decomposition.PCA(n_components=3)
# pca.fit(X)
# X = pca.transform(X)

# clf = LogisticRegression(random_state=0).fit(X_train, y_train_cat)
# yhat_log = clf.predict_proba(X_test)

# yhat_log_1 = yhat_log[:,1]>0.5

# plt.plot(yhat_log_1, '.b')
# plt.plot(y_test_cat, '*r')
    
# class_metrics(y_test_cat, y_pred_cat)





polynomial_features= PolynomialFeatures(degree=2)

x_poly = polynomial_features.fit_transform(x)
X_train_poly, X_test_poly, y_train_poly, y_test_poly = train_test_split(x_poly, y, test_size=0.2, random_state=42)



model2 = LinearRegression()
model2.fit(X_train_poly, y_train_poly)
y_poly_pred = model2.predict(X_test_poly)


mae_poly2 = mean_absolute_error(y_test_poly, y_poly_pred)


plt.plot(y_test_poly[30:60])
plt.plot(y_poly_pred[30:60])


class_metrics(y_test_poly, y_poly_pred)



#=========================test condn ===========

# test_a = np.arange(1,21)
# test_a = test_a.reshape((-1,2))
# test_a_poly = polynomial_features.fit_transform(test_a)


from sklearn.model_selection import KFold


kf = KFold(n_splits=10, random_state=42)
kf.get_n_splits(x)
mae_rfr_kf = []
y_pred_rfr_kf_list = []
y_test_kf_list = []
for train_index, test_index in kf.split(x):
    X_train_kf, X_test_kf = x[train_index], x[test_index]
    y_train_kf, y_test_kf = y[train_index], y[test_index]
    model_rfr_kf = RandomForestRegressor(max_depth=10, random_state=0)
    model_rfr_kf.fit(X_train_kf, y_train_kf)
    y_pred_rfr_kf = model_rfr.predict(X_test_kf)
    mae_rfr_kf.append(mean_absolute_error(y_test_kf, y_pred_rfr_kf))
    y_pred_rfr_kf_list.append(y_pred_rfr_kf)
    y_test_kf_list.append(y_test_kf)
    # cm = class_metrics(y_test_kf, y_pred_rfr_kf)
    # tn_kf.append(cm[0])
    # fp_kf.append(cm[1])
    # fn_kf.append(cm[2])
    # tp_kf.append(cm[3])


# y_test_kf_1_array = np.concatenate(y_test_kf_list, axis=0)
# y_pred_rfr_kf_1_array = np.concatenate(y_pred_rfr_kf_list, axis=0)

# class_metrics(y_test_kf_1_array, y_pred_rfr_kf_1_array)

############



from sklearn.ensemble import RandomForestRegressor

kf = KFold(n_splits=15, random_state=41)
mae_rfr_list = []
kf.get_n_splits(x)
y_pred_rfr_kf_list = []
y_test_kf_list = []
for train_index, test_index in kf.split(x):
    X_train_kf, X_test_kf = x[train_index], x[test_index]
    y_train_kf, y_test_kf = y[train_index], y[test_index]
    model_rfr = RandomForestRegressor(max_depth=15, random_state=0)
    model_rfr.fit(X_train_kf, y_train_kf)
    y_pred_rfr = model_rfr.predict(X_test_kf)
    mae_rfr_kf = mean_absolute_error(y_test_kf, y_pred_rfr)
    mae_rfr_list.append(mae_rfr_kf)
    y_pred_rfr_kf_list.append(y_pred_rfr)
    y_test_kf_list.append(y_test_kf)
    print("mae for rf: ", mae_rfr_kf)



y_test_kf_1_array = np.concatenate(y_test_kf_list, axis=0)
y_pred_rfr_kf_1_array = np.concatenate(y_pred_rfr_kf_list, axis=0)


mae_total = mean_absolute_error(y_test_kf_1_array, y_pred_rfr_kf_1_array)

class_metrics(y_test_kf_1_array, y_pred_rfr_kf_1_array)