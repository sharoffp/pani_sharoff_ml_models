#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split

from data_clean import data_cleaning



learning_rate = 0.01
training_epochs = 1000
display_step = 50

## Data

# with open('y.pickle', 'rb') as f:
#     # The protocol version used is detected automatically, so we do not
#     # have to specify it.
#     pickle_y = pickle.load(f)


# with open('y.pickle', 'rb') as f:
#     # The protocol version used is detected automatically, so we do not
#     # have to specify it.
#     pickle_x = pickle.load(f)

#if no pickle files available
#pickle_x, pickle_y = data_cleaning()

pickle_x = pickle_x[:,:80]

print(pickle_x.shape, pickle_y.shape)

train_X, test_X, train_Y, test_Y = train_test_split(pickle_x,pickle_y, test_size=0.2, random_state=42)


rng = numpy.random

n_samples = train_X.shape[0]


tf.compat.v1.disable_eager_execution()

m = train_X.shape[0]
n_x = train_X.shape[1]
n_y = 1

b_rand = rng.random()
b_rand = np.float64(b_rand)


# tf Graph Input
X = tf.placeholder(shape=[m,n_x],dtype=tf.float64)
Y = tf.placeholder(shape=[1, n_y],dtype=tf.float64)

# Set model weights
W = tf.Variable(rng.randn(train_X.shape[1],1), name="weight")
b = tf.Variable(b_rand, name="bias")

# constructing simple model
pred = tf.add(tf.matmul(X, W), b)

# Mean squared error
cost = tf.reduce_sum(tf.pow(pred-Y, 2))/(2*n_samples)
# Gradient descent
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()


# Start training
with tf.Session() as sess:
    sess.run(init)

    # Fit all training data
    for epoch in range(training_epochs):
        for (x, y) in zip(train_X, train_Y):
            sess.run(optimizer, feed_dict={X: x, Y: y})

        #Display logs per epoch step
        if (epoch+1) % display_step == 0:
            c = sess.run(cost, feed_dict={X: train_X, Y:train_Y})
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c), "W=", sess.run(W), "b=", sess.run(b))

    print("Optimization done")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    print("Training cost=", training_cost, "W=", sess.run(W), "b=", sess.run(b), '\n')

    # #Graphic display
    # plt.plot(train_X, train_Y, 'ro', label='Original data')
    # plt.plot(train_X, sess.run(W) * train_X + sess.run(b), label='Fitted line')
    # plt.legend()
    # plt.show()