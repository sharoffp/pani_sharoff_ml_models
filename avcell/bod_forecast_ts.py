#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import pandas
import matplotlib.pyplot as plt


import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval
from scipy.ndimage.filters import uniform_filter1d



excel_data_df = pandas.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')


#Filtering the data
 
excel_data_df = excel_data_df.iloc[7:,:]

excel_data_df2 = excel_data_df.fillna(method='ffill')



#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df2 = excel_data_df2.drop(excel_data_df2.columns[[remove_columns]], axis=1)

excel_data_df3 = excel_data_df2.iloc[3:,:]

rem_cols = [37,70]
excel_data_df3 = excel_data_df3.drop(excel_data_df3.columns[[rem_cols]], axis=1)

a = excel_data_df3.isnull().sum()

# df1 = df1.drop(df1.columns[[0]],axis = 1)

title = excel_data_df3.columns.tolist()


excel_data_df3 = excel_data_df3.fillna(method='bfill')


df_array = excel_data_df3.values


test_array = df_array[0,:]
df_test_array = np.array(test_array, dtype=np.float)

df_array[780,14] = df_array[781,14]
df_array[780,24] = df_array[781,24]



for i in range(len(df_array)):
    print(i)
    test_array = df_array[i,:]
    df_test_array = np.array(test_array, dtype=np.float)



df_array_fl = np.array(df_array[:,:], dtype=np.float)
diff_df = np.diff(df_array_fl,axis=0)



def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


data_train, data_test = split_data(diff_df)


forecast_days = 5

model = VAR((data_train))    
model_fit = model.fit(14)    
ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)

ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]


Quantity_interested = 16
 #   ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
  #  ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2
fig, ax = plt.subplots()
x = np.arange(5,5+forecast_days+1)
ax.plot(np.arange(1,5),data_train[:-5, Quantity_interested])
ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
ax.set_title("{}".format(title[Quantity_interested]))
ax.legend()
# fig.savefig("./{}.pdf".format(title[Quantity_interested]))


initial_value = np.reshape(df_array_fl[0,:],(1,-1))
conc_data_train = np.concatenate((initial_value,data_train), axis=0)
inv_data_train = np.cumsum(conc_data_train, axis = 0)

init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
conc_data_forecast = np.concatenate((init_forecast,ye_95_for), axis=0)
conc_data_fore_ciu = np.concatenate((init_forecast,ye_95_for_u), axis=0)
conc_data_fore_cil = np.concatenate((init_forecast,ye_95_for_l), axis=0)
conc_data_test = np.concatenate((init_forecast,data_test), axis=0)
    
    
inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
inv_test_data = np.cumsum(conc_data_test, axis = 0)


fig, ax = plt.subplots()
x = np.arange(1,forecast_days+1)
print(np.shape(x))
ax.plot(x,inv_forecast[:forecast_days,Quantity_interested], color='turquoise',label="pred")
ax.plot(x,inv_test_data[:forecast_days,Quantity_interested], color = 'k', label="true")
    # ax.fill_between(x,inv_forecast_l[:forecast_days,Quantity_interested], inv_forecast_u[:forecast_days,Quantity_interested], color='b', alpha=.1)
    # ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    # ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    # ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
ax.set_title("Label: {a} Index{b}".format(a=hard_title[hard_title_index],b=Quantity_interested))
ax.legend()
    fig.savefig("./plots/rm_plots_min/{}.pdf".format(title[Quantity_interested]))


# Selected plots: 5, 12, 14

plt.plot(excel_data_df.iloc[0:350,15])

excel_data_df.iloc[2,15]

title  = df_csv.iloc[0,:]
title = np.array(title)
title = title[1:]

# df1 = df1.drop([0],axis=0)
df_array = df1.to_numpy()



df_array_hr = df_array

print(np.shape(df_array_hr))
# df_array_hr = df_array
df_array_hr = np.array(df_array_hr, dtype=np.float)


N = 30
run_mean = running_mean(df_array_hr,N)
print("running mean")


run_mean = run_mean[range(0,len(run_mean),30),:]

#Differencing to avoid seasonality
diff_array = np.diff(run_mean,axis=0)


cut_off_range = len(df_array_hr)

#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)

s = pd.Series([0, 2, np.nan, np.nan, np.nan, 8])
s.interpolate(method='linear', axis=0)


#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df2 = excel_data_df2.drop(remove_columns, axis=0)



#Questions reg. data

# What does it mean to have a nan in a lab dataset.
# What is 73 BOD


#When is the reading already taken just so we can clarify things

# Also, daily are they taking the sample from the same time.









#3 days
def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test



#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n


#Parameters
forecast_days = 24
order = 72




df_xlsx = pd.read_excel("./data/new_metrics_data.xls")

title = df_xlsx.columns.tolist()

df_array = df_xlsx.to_numpy()
df_array_hr = df_array[range(0,len(df_array),60),:]

diff_array = np.diff(df_array_hr,axis=0)

cut_off_range = len(df_array_hr)
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 


ratio = (len(df_ar_no_out)-forecast_days)/len(df_ar_no_out)


#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out, ratio =ratio)



# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test



# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)


org_train = data_train
org_test = data_test


# org_train = data_train_reshaped
# org_val = data_val_reshaped

n_input = 48
parameter = 0
n_output = forecast_days


#org
org_train_arange = np.array(np.split(org_train,1))
org_val_arange = np.array(np.split(org_test,1))


# #SL
# x,y = to_supervised(train_arange,n_input)
# val_x,val_y = to_supervised(val_arange,n_input)

from numpy import array
from numpy import split


org_x, org_y = to_supervised(org_train_arange,n_input,n_out=n_output,req_parameter=parameter)
org_val_x, org_val_y = to_supervised(org_val_arange,n_input,n_out=n_output,req_parameter=parameter)



#Naming

org_train_x = org_x

# verbose, epochs, batch_size = 1, 20, 10
# n_timesteps, n_features, n_outputs = x.shape[1], x.shape[2], y.shape[1]

org_n_timesteps, org_n_features, org_n_outputs = org_train_x.shape[1], org_train_x.shape[2], org_y.shape[1]


print("n_output:", org_n_outputs)
# reshape output into [samples, timesteps, features]
# train_y = y.reshape((y.shape[0], y.shape[1], 1))
# val_y = val_y.reshape((val_y.shape[0], val_y.shape[1],1))

org_train_y = org_y.reshape((org_y.shape[0], org_y.shape[1], 1))
# org_val_y = org_val_y.reshape((org_val_y.shape[0], org_val_y.shape[1],1))


start_time = time.time()

# define model
# model = Sequential()
# model.add(LSTM(200, activation='relu', input_shape=(org_n_timesteps, org_n_features)))
# model.add(RepeatVector(org_n_outputs))
# model.add(LSTM(200, activation='relu', return_sequences=True))
# model.add(TimeDistributed(Dense(100, activation='relu')))
# model.add(TimeDistributed(Dense(1)))


# # define model l=== original ====
# model = Sequential()
# model.add(LSTM(10, activation='relu', input_shape=(org_n_timesteps,org_n_features)))
# model.add(RepeatVector(org_n_outputs))
# model.add(LSTM(10, activation='relu', return_sequences=True))
# model.add(TimeDistributed(Dense(10, activation='relu')))
# # model.add(TimeDistributed(Dense(5, activation='relu')))
# model.add(TimeDistributed(Dense(1)))

from keras.layers import Flatten

model = Sequential()
model.add(LSTM(50, activation='relu', input_shape=(org_n_timesteps,org_n_features)))
model.add(RepeatVector(org_n_outputs))
model.add(LSTM(250, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(9, activation='relu')))
model.add(TimeDistributed(Dense(5, activation='relu')))
model.add(TimeDistributed(Dense(1)))



epochs = 200
batch_size = len(org_train_x)
print("batch_size",batch_size)
verbose = 1

# log_dir = "logs/trial/final/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)




model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(org_train_y),np.shape(org_train_x))

print("Shape of val_y & val_x:",np.shape(org_val_y),np.shape(org_val_x))

# model.fit(org_train_x, org_train_y,validation_data=(org_val_x, org_val_y), epochs=epochs, batch_size=batch_size, verbose=verbose)

#  =======================
# model.fit(org_train_x, org_train_y, epochs=epochs, batch_size=batch_size, verbose=verbose)
# =========================

# j = -5

# x_test = org_val_x[j,:,:]
# x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))

# yhat_arange = model.predict(x_test_reshape, verbose=0)
# ytest_arange = org_val_y[j]

# print("pred: ", yhat_arange)
# print("true:", ytest_arange)

# training_time = start_time - time.time()
# print("Total time taken", training_time)

# org_pred = []

# for i in range(0,20):
#     x_test = org_val_x[i,:,:]
#     x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))
#     yhat_arange = model.predict(x_test_reshape, verbose=0)
#     # ytest_arange = org_val_y[j]
#     org_pred.append(yhat_arange)

# org_pred = np.array(org_pred)
# org_pred = org_pred.reshape(-1,1)

# plt.plot(org_pred[:20],label="pred")
# plt.plot(org_val_y[:20,0,0], label = "true")
# plt.title("val")
# plt.legend()
# plt.show()



# train_pred = []
# x
# for i in range(0,21):
#     x_train_test = org_train_x[i,:,:]
#     x_train_test_reshape = x_train_test.reshape((1,x_train_test.shape[0],x_train_test.shape[1]))
#     yhat_train_arange = model.predict(x_train_test_reshape, verbose=0)
#     # ytest_arange = org_val_y[j]
#     train_pred.append(yhat_train_arange)

# train_pred = np.array(train_pred)
# train_pred = train_pred.reshape(-1,1)

# plt.plot(train_pred[:20],label="pred")
# plt.plot(org_train_y[:20,0,0], label = "true")
# plt.title("train")
# plt.legend()
# plt.show()

# i=1
# x_test = org_val_x[i,:,:]
# x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))
# yhat_arange = model.predict(x_test_reshape, verbose=0)


# y_pred = yhat_arange.reshape((-1,1))
# y_test = org_val_y.reshape((-1,1))

# plt.plot(y_pred)
# plt.plot(y_test)
# plt.show()


# def inv_data(initial_value,diff_value, original_stream):
#     initial_value = np.reshape(original_stream[0,:],(1,-1))
#     concat_diff = np.concatenate((initial_value,diff_value), axis=0)
#     inv_data = np.cumsum(concat_diff, axis =0)
#     return inv_data


# initial_value = np.reshape(df_array_hr[0,:],(1,-1))
# conc_data_train = np.concatenate((initial_value,data_train), axis=0)
# inv_data_train = np.cumsum(conc_data_train, axis = 0)

# init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
# conc_data_forecast = np.concatenate((init_forecast,ye_95_for), axis=0)
# conc_data_fore_ciu = np.concatenate((init_forecast,ye_95_for_u), axis=0)
# conc_data_fore_cil = np.concatenate((init_forecast,ye_95_for_l), axis=0)
# conc_data_test = np.concatenate((init_forecast,data_test), axis=0)

    
# inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
# inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
# inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
# inv_test_data = np.cumsum(conc_data_test, axis = 0)


# order = 11
# parameter =7

# Quantity_interested = parameter
# model = VAR((data_train))    
# model_fit = model.fit(order)    
# ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
#  #   ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
#  #   ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
#   #  ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
#     #Extracting confidence interval
    
# ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]

# fig, ax = plt.subplots()
# x = np.arange(1,25)
# ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
# ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
# ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
# # ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
# # ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
# # ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
# ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
# ax.legend()
# # fig.savefig("./plots/{}.pdf".format(title[Quantity_interested]))



# initial_value = np.reshape(df_array_hr[0,:],(1,-1))
# conc_data_train = np.concatenate((initial_value,data_train), axis=0)
# inv_data_train = np.cumsum(conc_data_train, axis = 0)

# init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
# conc_data_forecast = np.concatenate((init_forecast,ye_95_for), axis=0)
# conc_data_fore_ciu = np.concatenate((init_forecast,ye_95_for_u), axis=0)
# conc_data_fore_cil = np.concatenate((init_forecast,ye_95_for_l), axis=0)
# conc_data_test = np.concatenate((init_forecast,data_test), axis=0)

    
# inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
# inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
# inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
# inv_test_data = np.cumsum(conc_data_test, axis = 0)

# fig, ax = plt.subplots()
# x = np.arange(1,26)
# ax.plot(x,inv_forecast[:,Quantity_interested], color='turquoise',label="pred")
# ax.plot(x,inv_test_data[:,Quantity_interested], color = 'k', label="true")
# ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
# ax.legend()


#Model fitting
def model_fitting(d_train, order,forecast_days = 14):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,min_order,max_order,forecast_days=14):
    mse_list = []
    for i in range(min_order,max_order+1):
        print(i)
        pred = model_fitting(d_train, i)
        # pred = pred[:,interest]
        d_test = d_test[:forecast_days,:]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    fig, ax = plt.subplots()
    x = np.arange(1,25)
    ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
    ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
# # ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
# # ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
# # ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
    ax.legend()
    fig.savefig("./plots/new_preprocessed_data/{}.pdf".format(title[Quantity_interested]))





minimum_order = 1
maximum_order = 40
Quantity_interested = 33


mse_list = model_selection(data_train, data_test, minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1
min_mse = np.min(mse_list,axis=0)
min_rmse = np.sqrt(min_mse)


mse_dictionary = {"mse":mse_list, "min_mse":min_mse_order}


import pickle
pickle.dump( mse_dictionary, open( "save_mse_min_01__max_40_6months_new_data.p", "wb" ) )

mse_dict = {}
min_mse_list = []


#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    min_order_for_Quantity = min_mse_order[kk]
    act_fit_ci(data_train,min_order_for_Quantity, Quantity_interested,forecast_days=24)
    print("Order Choosen {} for {}".format(min_order_for_Quantity,title[kk]))
