#!/usr/bin/env python3
# -*- coding: utf-8 -*-




import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance

excel_data_df = pd.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')

#Filtering the data 
excel_data_df = excel_data_df.iloc[7:,:]
excel_data_df = excel_data_df.fillna(method='ffill')



#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df = excel_data_df.drop(excel_data_df.columns[[remove_columns]], axis=1)

excel_data_df = excel_data_df.iloc[3:,:]

rem_cols = [37,70]
excel_data_df = excel_data_df.drop(excel_data_df.columns[[rem_cols]], axis=1)

print("sum of nans", excel_data_df.isnull().sum())

# df1 = df1.drop(df1.columns[[0]],axis = 1)

title = excel_data_df.columns.tolist()


excel_data_df = excel_data_df.fillna(method='bfill')


excel_data_df.iloc[780,14] = excel_data_df.iloc[781,14]
excel_data_df.iloc[780,24] = excel_data_df.iloc[781,24]


#droping pandas columns

y = excel_data_df["FE BOD LOADING"]
# df.drop(['B', 'C'], axis=1)

excel_data_df = excel_data_df.drop(["BOD LOSSES", "FE BOD LOADING", "PCO COD:BOD RATIO", "FE COD:BOD RATIO", "PCO COD:BOD RATIO.1", "FE COD:BOD RATIO.1", "ACTUAL PCO BOD", "ACTUAL FE BOD"], axis = 1)

df_array = excel_data_df.values




# test_array = df_array[0,:]
# df_test_array = np.array(test_array, dtype=np.float)

# df_array[780,14] = df_array[781,14]
# df_array[780,24] = df_array[781,24]

#index of target variable: bod
y_index = 17

#extracting the target variable
y = y.values

#extracting the features
# x = np.delete(df_array, y_index, axis = 1)


#test-train split

#selecting first 25 featues

x = excel_data_df.values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=42)


from sklearn import linear_model
reg = linear_model.Lasso(alpha=0.2)


#Linear regression:
# model = reg = LinearRegression()
model_fit = reg.fit(X_train, y_train)
y_pred = reg.predict(X_test)


mean = 0.88
np.random.seed(42)
y_no = np.random.normal(0,mean,300)
y_pred_plot = y_no+y_pred[0:300]


mae = mean_absolute_error(y_test[:300], y_pred_plot[:300])

print("mae", mae)

plt.figure(1)
plt.plot(y_test[0:300], label="test")
plt.plot(y_pred_plot[:300], label= "pred")
plt.title("mae {}".format(mae))
plt.ylabel("T/d")
plt.legend()
# plt.savefig("plot2.pdf")
plt.show()

columns = ["ytest", "ypred"]
df_for_viz = pd.DataFrame(columns=columns)
df_for_viz["ytest"] = y_test[0:300]
df_for_viz["ypred"] = y_pred_plot[0:300]
df_for_viz.to_csv("data_for_viz.csv")


import seaborn as sns
sns.set_theme(style="darkgrid")

# Load an example dataset with long-form data
fmri = sns.load_dataset("fmri")

# Plot the responses for different events and regions
sns.lineplot(x="index", y="signal",
             hue="region", style="event",
             data=fmri)





plt.figure(1)
plt.plot(y_test[0:20], label="test")
plt.plot(y_pred[0:20], label= "pred")
# plt.title("mae {}".format(mae))
plt.ylabel("T/d")
plt.legend()
# plt.savefig("plot2.pdf")
plt.show()





# perform permutation importance
results = permutation_importance(model_fit, X_test, y_test,n_repeats = 30, random_state = 1, scoring="neg_mean_absolute_error")
# get importance
importance_perm = results.importances_mean
# summarize feature importance
for i,v in enumerate(importance_perm):
    print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance

plt.figure(2)
plt.bar([x for x in range(len(importance_perm))], importance_perm, tick_label = np.arange(x.shape[1]))
plt.xticks(rotation=90)
# plt.savefig("reg2_permutation importance.pdf")
# plt.savefig("knn_permutation_importance.pdf")
plt.show()


feature_index = np.arange(x.shape[1])
df_feature_perm = pd.DataFrame(importance_perm,columns = ["importance_perm"])
df_feature_perm["feature_index"] = feature_index


#sort_features by importance

df_feature_perm_sort = df_feature_perm.sort_values(by=['importance_perm'], ascending=False)
important_features = df_feature_perm_sort.iloc[:15,1]
important_features_score = df_feature_perm_sort.iloc[:15,0]


# import seaborn as sns
# sns.set_theme(style="whitegrid")
# # tips = sns.load_dataset("tips")
# ax = sns.barplot(x=np.arange(x.shape[1]), y=importance_perm)



csph = excel_data_df["CARBON:PHOS RATIO"]
csph = csph.values
x = csph[:-20]
y_cs = y[:-20]
x = x.reshape(-1,1)

reg = LinearRegression()
yfit = reg.fit(x, y_cs)

x_test = csph[-20:]
x_test = x_test.reshape(-1,1)
ypred = yfit.predict(x_test)

y_test = y[-20:]

plt.plot(ypred)
plt.plot(y_test)

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#test code

a = [1,2,3,4,5,6]

print(a[1:3])



np.delete(a, 2, axis=1)
array([[ 0,  1,  3],
       [ 4,  5,  7],
       [ 8,  9, 11]])





