#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from numpy import array

from statsmodels.graphics.tsaplots import plot_pacf
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import PolynomialFeatures


def class_metrics(y_true, y_pred):
    y_test_cat = y_true>5.5
    y_pred_cat = y_pred>5.5
    tn, fp, fn, tp = confusion_matrix(y_test_cat, y_pred_cat).ravel()
    recall = tp/(tp+fn)
    print("true positive", tp)
    print("total positive events", np.sum(y_test_cat))
    print("recall", recall)
    precision = tp/(tp+fp)
    print("total postive events in ypred", np.sum(y_pred_cat))
    print("precision", precision)
    return [tn, fp, fn, tp]


def data_cleaning():

    excel_data_df = pd.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')


#Filtering the data 
    excel_data_df = excel_data_df.iloc[7:,:]
# excel_data_df = excel_data_df.fillna(method='ffill')



#REMOVING unwanted These columns to avoid all the nans.
    remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

    excel_data_df = excel_data_df.drop(excel_data_df.columns[[remove_columns]], axis=1)

    excel_data_df = excel_data_df.iloc[3:,:]

    rem_cols = [37,70]
    excel_data_df = excel_data_df.drop(excel_data_df.columns[[rem_cols]], axis=1)
    print("sum of nans", excel_data_df.isnull().sum())

    FE_BOD_LOADING_NAN = excel_data_df[excel_data_df['FE BOD LOADING'].isnull()]
    FE_BOD_LOADING_index = FE_BOD_LOADING_NAN.index
    FE_BOD_LOADING_index_array = np.array(FE_BOD_LOADING_index)

#unwanted strings
    excel_data_df.iloc[780,14] = excel_data_df.iloc[781,14]
    excel_data_df.iloc[780,24] = excel_data_df.iloc[781,24]

    excel_data_df_float = excel_data_df.astype('float64', copy = True, errors='raise')

    excel_data_df_float = excel_data_df_float.interpolate(method='spline',order = 2, axis = 0)
    print("checking if any other nans", excel_data_df_float.isnull().sum())

#dropping rows where bod is nan
    excel_data_df_float = excel_data_df_float.drop(FE_BOD_LOADING_index_array)

    excel_data_df = excel_data_df_float

#droping pandas columns

    y = excel_data_df["FE BOD LOADING"]

#extracting the target variable
    y = y.values

# df.drop(['B', 'C'], axis=1)

    excel_data_df = excel_data_df.drop(["BOD LOSSES", "FE BOD LOADING", "PCO COD:BOD RATIO", "FE COD:BOD RATIO", "PCO COD:BOD RATIO.1", "FE COD:BOD RATIO.1", "ACTUAL PCO BOD", "ACTUAL FE BOD"], axis = 1)

# "TSS DAILY LIMIT"

    excel_data_df = excel_data_df.drop(["BOD DAILY LIMIT", "BOD MONTHLY LIMIT" , "TSS MONTHLY LIMIT", "Unnamed: 148", "Unnamed: 150", "Unnamed: 151", "Unnamed: 152", "Unnamed: 156", "Unnamed: 157", "Unnamed: 159"], axis =1)

    excel_data_df = excel_data_df.drop(["FE BOD"], axis =1)

    excel_data_df = excel_data_df.drop(["FE BOD 7-d roll. Avg", "DISCHARGE kg of BOD/MT", "DISCHARGE kg of BOD/MT.1"], axis =1)

    excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.2", "PCO LOADING kg of BOD/MT" , "PCO BOD.1"], axis =1)

    excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.1"], axis =1)

    excel_data_df = excel_data_df.drop(["PCO BOD"], axis =1)




    excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

    excel_data_df = excel_data_df.drop(["Carbon to Plant", "Carbon used by bugs", "CARBON: NITROGEN RATIO"],axis =1)
    excel_data_df = excel_data_df.drop([ "CARBON:PHOS RATIO", "BOD", "BOD.1", "TSS.1", "TSS.2", "TSS", "DISCHARGE kg of TSS/MT", "DISCHARGE kg of TSS/MT.1", "BOD.3", "PCO BOD.2"], axis =1)

# excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)


    excel_data_df = excel_data_df.drop([' TSS DAILY LIMIT'], axis =1)


    excel_data_df = excel_data_df.drop(['Unnamed: 149', 'Unnamed: 160', 'Unnamed: 161'], axis =1)

    excel_data_df = excel_data_df.drop(['NITROGEN:PHOS RATIO'], axis =1)


#limits
    excel_data_df = excel_data_df.drop(['Unnamed: 80'], axis =1)

    excel_data_df = excel_data_df.drop(['Unnamed: 76'], axis =1)

    excel_data_df = excel_data_df.drop(['Unnamed: 82'], axis =1)

    excel_data_df = excel_data_df.drop(['Unnamed: 86'], axis =1)


    excel_data_df = excel_data_df.drop(['Unnamed: 153', 'Unnamed: 154'], axis =1)


# excel_data_df = excel_data_df.drop(['FE COD.2'], axis =1)
    excel_data_df = excel_data_df.drop(['Unnamed: 74', 'Unnamed: 84'], axis =1)

    excel_data_df = excel_data_df.drop(['BOD.2'], axis =1)

# excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

# "PCO LOADING OF BOD/MT.1"
#renaming features
    excel_data_df = excel_data_df.rename(columns = {"Unnamed: 1": "Effluent Flow", "Unnamed: 144": "PCO COD 7-d roll avg"})

# excel_data_df['NITROGEN:PHOS RATIO'] = excel_data_df_float['NITROGEN:PHOS RATIO']

#one hot encoding

# Nitrogen_phos_ratio = pd.get_dummies(excel_data_df['NITROGEN:PHOS RATIO'], prefix = 'NITROGEN:PHOS RATIO')


#removing this because mostly 20 and only few values (unstable)
# excel_data_df = excel_data_df.drop(['NITROGEN:PHOS RATIO'], axis =1)

    feature_list = excel_data_df.columns.tolist()
    feature_array = np.array(feature_list)

#pco loading kg of BOD - how do they calculate this bod
    x = excel_data_df.values
#to remove one nan values on top
    x = x[1:,:]
    y = y[1:]
    return x,y
    