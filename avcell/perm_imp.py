#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance

excel_data_df = pd.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')

#Filtering the data 
excel_data_df = excel_data_df.iloc[7:,:]
excel_data_df = excel_data_df.fillna(method='ffill')

#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df = excel_data_df.drop(excel_data_df.columns[[remove_columns]], axis=1)

excel_data_df = excel_data_df.iloc[3:,:]

rem_cols = [37,70]
excel_data_df = excel_data_df.drop(excel_data_df.columns[[rem_cols]], axis=1)

print("sum of nans", excel_data_df.isnull().sum())

# df1 = df1.drop(df1.columns[[0]],axis = 1)

title = excel_data_df.columns.tolist()

excel_data_df = excel_data_df.fillna(method='bfill')

excel_data_df.iloc[780,14] = excel_data_df.iloc[781,14]
excel_data_df.iloc[780,24] = excel_data_df.iloc[781,24]


#droping pandas columns

# df.drop(['B', 'C'], axis=1)

excel_data_df = excel_data_df.drop(["BOD LOSSES", "FE BOD LOADING", "PCO COD:BOD RATIO", "FE COD:BOD RATIO", "PCO COD:BOD RATIO.1", "FE COD:BOD RATIO.1", "ACTUAL PCO BOD", "ACTUAL FE BOD"], axis = 1)

df_array = excel_data_df.values

# test_array = df_array[0,:]
# df_test_array = np.array(test_array, dtype=np.float)

# df_array[780,14] = df_array[781,14]
# df_array[780,24] = df_array[781,24]

#index of target variable: bod
y_index = 17

#extracting the target variable
y = df_array[:,y_index]

#extracting the features
x = np.delete(df_array, y_index, axis = 1)


#test-train split

#selecting first 25 featues
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=42)


#Linear regression:
model = reg = LinearRegression()
model_fit = model.fit(X_train, y_train)
y_pred = model.predict(X_test)

# perform permutation importance
results = permutation_importance(model_fit, X_test, y_test,n_repeats = 30, random_state = 1, scoring="neg_mean_absolute_error")
# get importance
importance_perm = results.importances_mean
# summarize feature importance
for i,v in enumerate(importance_perm):
    print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance

plt.figure(2)
plt.bar([x for x in range(len(importance_perm))], importance_perm, tick_label = np.arange(x.shape[1]))
plt.xticks(rotation=90)
# plt.savefig("reg2_permutation importance.pdf")
# plt.savefig("knn_permutation_importance.pdf")
plt.show()

feature_index = np.arange(x.shape[1])
df_feature_perm = pd.DataFrame(importance_perm,columns = ["importance_perm"])
df_feature_perm["feature_index"] = feature_index

#sort_features by importance

df_feature_perm_sort = df_feature_perm.sort_values(by=['importance_perm'], ascending=False)
important_features = df_feature_perm_sort.iloc[:15,1]
important_features_score = df_feature_perm_sort.iloc[:15,0]
