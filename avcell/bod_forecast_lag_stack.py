#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from numpy import array

from statsmodels.graphics.tsaplots import plot_pacf
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression

from sklearn.inspection import permutation_importance


from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from keras.models import load_model
from keras.utils import to_categorical
from numpy import dstack


excel_data_df = pd.read_excel('op_data_jan15_to_june20.xlsx', sheet_name='Oper. Report')

#Filtering the data 
excel_data_df = excel_data_df.iloc[7:,:]
excel_data_df = excel_data_df.fillna(method='ffill')


#REMOVING These columns to avoid all the nans.
remove_columns = [0,36,40,41,42,57,58,77,87,88,97,98,99,100,101,102,103,104, 107, 111, 137, 138, 147, 155,158]

excel_data_df = excel_data_df.drop(excel_data_df.columns[[remove_columns]], axis=1)

excel_data_df = excel_data_df.iloc[3:,:]

rem_cols = [37,70]
excel_data_df = excel_data_df.drop(excel_data_df.columns[[rem_cols]], axis=1)

print("sum of nans", excel_data_df.isnull().sum())

# df1 = df1.drop(df1.columns[[0]],axis = 1)

title = excel_data_df.columns.tolist()


excel_data_df = excel_data_df.fillna(method='bfill')


excel_data_df.iloc[780,14] = excel_data_df.iloc[781,14]
excel_data_df.iloc[780,24] = excel_data_df.iloc[781,24]


#droping pandas columns

y = excel_data_df["FE BOD LOADING"]

#extracting the target variable
y = y.values

# df.drop(['B', 'C'], axis=1)

excel_data_df = excel_data_df.drop(["BOD LOSSES", "FE BOD LOADING", "PCO COD:BOD RATIO", "FE COD:BOD RATIO", "PCO COD:BOD RATIO.1", "FE COD:BOD RATIO.1", "ACTUAL PCO BOD", "ACTUAL FE BOD"], axis = 1)

# "TSS DAILY LIMIT"

excel_data_df = excel_data_df.drop(["BOD DAILY LIMIT", "BOD MONTHLY LIMIT" , "TSS MONTHLY LIMIT", "Unnamed: 148", "Unnamed: 150", "Unnamed: 151", "Unnamed: 152", "Unnamed: 156", "Unnamed: 157", "Unnamed: 159"], axis =1)

excel_data_df = excel_data_df.drop(["FE BOD"], axis =1)

excel_data_df = excel_data_df.drop(["FE BOD 7-d roll. Avg", "DISCHARGE kg of BOD/MT", "DISCHARGE kg of BOD/MT.1"], axis =1)

excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.2", "PCO LOADING kg of BOD/MT" , "PCO BOD.1"], axis =1)

excel_data_df = excel_data_df.drop(["PCO LOADING kg of BOD/MT.1"], axis =1)

excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)

excel_data_df = excel_data_df.drop(["Carbon to Plant", "Carbon used by bugs", "CARBON: NITROGEN RATIO"],axis =1)
excel_data_df = excel_data_df.drop([ "CARBON:PHOS RATIO", "BOD", "BOD.1", "TSS.1", "TSS.2", "TSS", "DISCHARGE kg of TSS/MT", "DISCHARGE kg of TSS/MT.1", "BOD.3", "PCO BOD.2"], axis =1)

# excel_data_df = excel_data_df.drop(["Carbon out of Plant", "PCO BOD 7-d roll. Avg"], axis =1)


# "PCO LOADING OF BOD/MT.1"
#renaming features
excel_data_df = excel_data_df.rename(columns = {"Unnamed: 1": "Effluent Flow", "Unnamed: 144": "PCO COD 7-d roll avg"})



feature_list = excel_data_df.columns.tolist()
feature_array = np.array(feature_list)
#values like nitrogen:phos ratio make it as categorical value because 0 or 20 ?

df_array = excel_data_df.values

#adding the bod lag inputs

lag = 10

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
    # flatten data
    data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
    X, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(data)):
        # define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_out
        # ensure we have enough data for this instance
        if out_end <= len(data):
            X.append(data[in_start:in_end, :])
            y.append(data[in_end:out_end, req_parameter])
            # move along one time step
        in_start += 1
    return np.array(X), np.array(y)


plot_pacf(y)

y_sup_input = np.reshape(y,(-1,1,1)) #check to_supervised for dataformat
lag_bod, current_bod = to_supervised(y_sup_input,lag, 1, 0)

lag_bod = np.reshape(lag_bod,(-1,lag)) #check to_supervised for dataformat

#5 day output
bod_prev_5days = lag_bod[:,:5 ]



# test_array = df_array[0,:]
# df_test_array = np.array(test_array, dtype=np.float)

# df_array[780,14] = df_array[781,14]
# df_array[780,24] = df_array[781,24]

#index of target variable: bod
y_index = 17


#extracting the features
# x = np.delete(df_array, y_index, axis = 1)


#test-train split

#selecting first 25 featues

x = excel_data_df.values


y = y[10:]

x = x[10:, :]


x = np.append(x, bod_prev_5days, axis=1)



from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)


from sklearn import linear_model
reg = linear_model.Lasso(alpha=0.2)


#Linear regression:
# model = reg = LinearRegression()
model_fit = reg.fit(X_train, y_train)
y_pred = reg.predict(X_test)


mae = mean_absolute_error(y_test, y_pred)

print("mae", mae)

plt.figure(1)
plt.plot(y_test, label="test")
plt.plot(y_pred, label= "pred")
plt.title("mae {}".format(mae))
plt.ylabel("T/d")
plt.legend()
# plt.savefig("plot2.pdf")
plt.show()


# perform permutation importance
results = permutation_importance(model_fit, X_test, y_test,n_repeats = 30, random_state = 1, scoring="neg_mean_absolute_error")
# get importance
importance_perm = results.importances_mean
# summarize feature importance
for i,v in enumerate(importance_perm):
    print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance

# plt.figure(2)
# plt.bar([x for x in range(len(importance_perm))], importance_perm, tick_label = np.arange(x.shape[1]))
# plt.xticks(rotation=90)
# # plt.savefig("reg2_permutation importance.pdf")
# # plt.savefig("knn_permutation_importance.pdf")
# plt.show()


feature_index = np.arange(x.shape[1])
df_feature_perm = pd.DataFrame(importance_perm,columns = ["importance_perm"])
df_feature_perm["feature_index"] = feature_index


#sort_features by importance

df_feature_perm_sort = df_feature_perm.sort_values(by=['importance_perm'], ascending=False)
important_features = df_feature_perm_sort.iloc[:15,1]
important_features_score = df_feature_perm_sort.iloc[:15,0]
list_important_featues = important_features.tolist()

print("IMPORTANT FEATURES BY ORDER", feature_array[list_important_featues])


# csph = excel_data_df["CARBON:PHOS RATIO"]
# csph = csph.values
# x = csph[:-20]
# y_cs = y[:-20]
# x = x.reshape(-1,1)

# reg = LinearRegression()
# yfit = reg.fit(x, y_cs)

# x_test = csph[-20:]
# x_test = x_test.reshape(-1,1)
# ypred = yfit.predict(x_test)

# y_test = y[-20:]

# plt.plot(ypred)
# plt.plot(y_test)


#saving sub-models for later use in a stacking ensemble
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense
from matplotlib import pyplot
from os import makedirs

 
# fit model on dataset
def fit_model(trainX, trainy):
	# define model
	model = Sequential()
	model.add(Dense(14, input_dim=trainX.shape[1], activation='relu'))
	model.add(Dense(7, activation='relu'))
	model.add(Dense(1))
	model.compile(loss='mse', optimizer='adam')
	# fit model
	model.fit(trainX, trainy, epochs=500, verbose=0)
	return model


trainX, testX = X_train, X_test
trainy, testy = y_train, y_test


trainX = np.asarray(trainX).astype('float32')
trainy = np.asarray(trainy).astype('float32')
testX = np.asarray(testX).astype('float32')
testy = np.asarray(testy).astype('float32')


print(trainX.shape, testX.shape)
# create directory for models
makedirs('models1')
# fit and save models
n_members = 5
for i in range(n_members):
	# fit model
	model = fit_model(trainX, trainy)
	# save model
	filename = 'models/model_' + str(i + 1) + '.h5'
	model.save(filename)
	print('>Saved %s' % filename)

 
# load models from file
def load_all_models(n_models):
	all_models = list()
	for i in range(n_models):
		# define filename for this ensemble
		filename = 'models/model_' + str(i + 1) + '.h5'
		# load model from file
		model = load_model(filename)
		# add to list of members
		all_models.append(model)
		print('>loaded %s' % filename)
	return all_models
 
# create stacked model input dataset as outputs from the ensemble
def stacked_dataset(members, inputX):
	stackX = None
	for model in members:
		# make prediction
		yhat = model.predict(inputX, verbose=0)
		# stack predictions into [rows, members, probabilities]
		if stackX is None:
			stackX = yhat
		else:
			stackX = dstack((stackX, yhat))
	# flatten predictions to [rows, members x probabilities]
	stackX = stackX.reshape((stackX.shape[0], stackX.shape[1]*stackX.shape[2]))
	return stackX
 
# fit a model based on the outputs from the ensemble members
def fit_stacked_model(members, inputX, inputy):
	# create dataset using ensemble
	stackedX = stacked_dataset(members, inputX)
	# fit standalone model
	model = LinearRegression()
	model.fit(stackedX, inputy)
	return model
 
# make a prediction with the stacked model
def stacked_prediction(members, model, inputX):
	# create dataset using ensemble
	stackedX = stacked_dataset(members, inputX)
	# make a prediction
	yhat = model.predict(stackedX)
	return yhat
 
n_members = 5
members = load_all_models(n_members)
print('Loaded %d models' % len(members))

model = fit_stacked_model(members, testX, testy)
# evaluate model on test set
yhat = stacked_prediction(members, model, testX)
mae = mean_absolute_error(testy, yhat)
print('Stacked Test Accuracy: %.3f' % mae)

plt.figure(3)
plt.plot(yhat[:20], '-k')
plt.plot(testy[:20], '.b')
plt.show()
