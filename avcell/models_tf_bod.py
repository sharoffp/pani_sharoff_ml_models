
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split

rng = numpy.random

with open('y_data.pickle', 'rb') as f:
    # The protocol version used is detected automatically, so we do not
    # have to specify it.
    pickle_y = pickle.load(f)

with open('x_data.pickle', 'rb') as f:
    pickle_x = pickle.load(f)

#if pickle files are not availale
# from data_clean import data_cleaning
# pickle_x, pickle_y = data_cleaning()


pickle_x = pickle_x[:,:80]

pickle_y = pickle_y.reshape((-1,1))


print(pickle_x.dtype)
print(pickle_y.dtype)

#perform dtype conversion depending on the weights and bias
# pickle_x = numpy.float64(pickle_x)
# pickle_y = numpy.float64(pickle_y)

train_X,test_X, train_Y, test_Y = train_test_split(pickle_x,pickle_y, test_size=0.2, random_state=42)


# Only if interested in categories
# y_train_cat = y_train>5.5
# y_test_cat = y_test>5.5

batch_size = 64
SHUFFLE_BUFFER_SIZE = 100


# Data
n_samples = train_X.shape[0]
row = train_X.shape[0]
column = train_X.shape[1]

print(row, column)

# tf Graph Input
X = tf.placeholder(tf.float32, [None, column])
Y = tf.placeholder(tf.float32, [None,1])

n_samples = train_X.shape[0]

# Hyper Parameters and others

learning_rate = 0.1
num_steps = 500
training_epochs = 20
batch_size = 128

#For summary writter in tfboard
logs_path = './tf_logs/test1'
display_step = 10

# Network1 Hyper Parameters
n_hidden_1 = 256 # 1st layer number of neurons
n_hidden_2 = 256 # 2nd layer number of neurons
num_classes = 1 # since we are predicting one value

# Network 2 Hyper Parameters
n_hidden_2_1 = 256 # 1st layer number of neurons
n_hidden_2_2 = 256 # 2nd layer number of neurons
num_classes_2 = 1 # since we are predicting one value

# Store layers weight & bias for network 1 and network 2
weights = {
    'h11': tf.Variable(tf.random_normal([column, n_hidden_1])),
    'h12': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out1': tf.Variable(tf.random_normal([n_hidden_2, num_classes])),
    'h21': tf.Variable(tf.random_normal([column, n_hidden_1])),
    'h22': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out2': tf.Variable(tf.random_normal([n_hidden_2, num_classes]))
}
biases = {
    'b11': tf.Variable(tf.random_normal([n_hidden_1])),
    'b12': tf.Variable(tf.random_normal([n_hidden_2])),
    'out1': tf.Variable(tf.random_normal([num_classes])),
    'b21': tf.Variable(tf.random_normal([n_hidden_2_1])),
    'b22': tf.Variable(tf.random_normal([n_hidden_2_2])),
    'out2': tf.Variable(tf.random_normal([num_classes_2])),
}

# Create model
def neural_net1(x):
    # Hidden fully connected layer with relu activation
    layer_1 = tf.add(tf.matmul(x, weights['h11']), biases['b11'])
    layer_1 = tf.nn.relu(layer_1)
    tf.summary.histogram("relu1", layer_1)
    # Hidden fully connected layer with relu activation
    layer_2 = tf.add(tf.matmul(layer_1, weights['h12']), biases['b12'])
    layer_2 = tf.nn.relu(layer_2)
    tf.summary.histogram("relu2", layer_2)
    # Output fully connected layer with no activation
    out_layer = tf.matmul(layer_2, weights['out1']) + biases['out1']
    return out_layer


def neural_net2(x):
    # Hidden fully connected layer with relu activation
    layer_1 = tf.add(tf.matmul(x, weights['h21']), biases['b21'])
    layer_1 = tf.nn.relu(layer_1)
    # Hidden fully connected layer with relu activation
    layer_2 = tf.add(tf.matmul(layer_1, weights['h22']), biases['b22'])
    layer_2 = tf.nn.relu(layer_2)
    # Output fully connected layer with no activation
    out_layer = tf.matmul(layer_2, weights['out2']) + biases['out2']
    return out_layer

# def neural_net3(x):
#     # Hidden fully connected layer with relu neurons
#     layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
#     layer_1 = tf.nn.relu(layer_1)
#     # Hidden fully connected layer with relu neurons
#     layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
#     layer_2 = tf.nn.relu(layer_2)
#     # Output fully connected with no activation
#     out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
#     return out_layer



# # Construct model

# logits1 = neural_net1(X)
def logits1(X):
    return neural_net1(X)


# logits2 = neural_net2(X)
def logits2(X):
    return neural_net2(X)

#cat
def logits3(X):
    return (logits1(X) +logits2(X))/2

stack = logits3(X)

# Define loss and optimizer

# Mean squared error
cost = tf.reduce_sum(tf.pow(logits3(X)-Y, 2))/(2*n_samples)

#use this for classificaiton
# loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(cost)

# # Evaluate model (with test logits, for dropout to be disabled)
# correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(Y, 1))
# accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# merged_summary_op = tf.summary.merge_all()
# Start training


# Create a summary to monitor cost tensor
tf.summary.scalar("loss", cost)

# Merge all summaries into a single op
merged_summary_op = tf.summary.merge_all()

with tf.Session() as sess:

    # Run the initializer
    sess.run(init)
    summary_writer = tf.summary.FileWriter(logs_path,graph=tf.get_default_graph())
    for epoch in range(training_epochs):
        for (x, y) in zip(train_X, train_Y):
            x = x.reshape((1,-1)) #passing it for the right place holders dimensions
            y = y.reshape((1,-1)) 
        # Run optimization op (backprop)
            sess.run(train_op, feed_dict={X: x, Y: y})
        if epoch % display_step == 0 or epoch == 1:
            # Calculate batch loss and accuracy
            loss = sess.run([cost], feed_dict={X: x, Y: y})
            # Write logs at every iteration
            # summary_writer.add_summary(loss, epoch)
            # summary_writer.add_summary(summary, epoch)
            print("Step " + str(epoch) + ", loss=  " ,loss)
            # tf.summary.scalar('accuracy', train_accuracy.result(), step=epoch)
    print("Optimization done")

# with train_summary_writer.as_default():
#     tf.summary.scalar('loss', train_loss.result(), step=epoch)
#     tf.summary.scalar('accuracy', train_accuracy.result(), step=epoch)

output = []

with tf.Session() as sess:
    sess.run(init)
    for test_points in range(test_X.shape[0]):
        test = test_X[test_points,:]
        test = test.reshape((1,-1))
        output.append(sess.run(stack, feed_dict={ X: test}))

# predictions = logits3.eval(feed_dict = {x:test_X})
# with sess.as_default():   # or `with sess:` to close on exit
#     prediction = sess.run(stack,feed_dict = {X: test_X})
# assert sess is tf.get_default_session()
#     assert t.eval() == sess.run(t)
















