#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split

rng = numpy.random

# Parameters
learning_rate = 0.01
training_epochs = 1000
display_step = 50


with open('y_data.pickle', 'rb') as f:
    pickle_y = pickle.load(f)


with open('x_data.pickle', 'rb') as f:
    # The protocol version used is detected automatically, so we do not
    # have to specify it.
    pickle_x = pickle.load(f)


# from data_clean import data_cleaning
# pickle_x, pickle_y = data_cleaning()


pickle_x = pickle_x[:,:80]
pickle_y = pickle_y.reshape((-1,1))

train_X,test_X, train_Y, test_Y = train_test_split(pickle_x,pickle_y, test_size=0.2, random_state=42)


# y_train_cat = y_train>5.5
# y_test_cat = y_test>5.5

batch_size = 64
SHUFFLE_BUFFER_SIZE = 100


# Data
n_samples = train_X.shape[0]
row = train_X.shape[0]
column = train_X.shape[1]

print(row, column)

# tf Graph Input
X = tf.placeholder(tf.float64, [None, column])
Y = tf.placeholder(tf.float64, [None,1])

# Set model weights
W = tf.Variable(rng.randn(column,1), name="weight")
b = tf.Variable(rng.randn(1), name="bias")

# W = tf.Variable(tf.zeros([column, 1]), name="weight")
# b = tf.Variable(tf.zeros([1]), name="bias")

# Construct a simple model
pred = tf.add(tf.matmul(X, W), b)

# Mean squared error
cost = tf.reduce_sum(tf.pow(pred-Y, 2))/(2*n_samples)

# Gradient descent
#  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
# optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)


#Note: Adam for better convergence
optimizer = tf.train.AdamOptimizer().minimize(cost)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

print("shape of train X", train_X.shape)

# Start training
with tf.Session() as sess:

    # Run the initializer
    sess.run(init)

    # Fit all training data
    for epoch in range(training_epochs):
        for (x, y) in zip(train_X, train_Y):
            # print("shape of x", x.shape, "shape of y", y.shape)
            x = x.reshape((1,-1))
            y = y.reshape((1,-1))
            sess.run(optimizer, feed_dict={X: x, Y: y})

        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
            c = sess.run(cost, feed_dict={X: train_X, Y:train_Y})
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c),"W=", sess.run(W), "b=", sess.run(b))

    print("Optimization Finished!")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    print("Training cost=", training_cost, "W=", sess.run(W), "b=", sess.run(b), '\n')

    # Graphic display
    plt.plot(train_Y, 'ro', label='Original data')
    W_array = sess.run(W)
    b_array = sess.run(b)
    print("type of W_array", type(W_array), "shape of W_array", W_array.shape)
    y_train_pred = numpy.matmul(train_X, W_array) + b_array
    plt.figure(1)
    plt.plot(train_Y, 'bo', label='Training data')
    plt.plot(y_train_pred, label='Fitted line')
    plt.legend()
    plt.show()

    print("Testing... (Mean square loss Comparison)")
    testing_cost = sess.run(tf.reduce_sum(tf.pow(pred - Y, 2)) / (2 * test_X.shape[0]), feed_dict={X: test_X, Y: test_Y})  # same function as cost above
    print("Testing cost=", testing_cost)
    print("Absolute mean square loss difference:", abs(training_cost - testing_cost))

    y_test_pred = numpy.matmul(test_X, W_array) + b_array    
    plt.figure(2)
    plt.plot(test_Y, 'bo', label='Testing data')
    plt.plot(y_test_pred, label='Fitted line')    
    # plt.plot(, label='Fitted line')
    plt.legend()
    plt.show()