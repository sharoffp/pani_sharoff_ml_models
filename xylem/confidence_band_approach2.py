
import numpy as np
import stat

import pandas as pd
import matplotlib.pyplot as plt


#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error 
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

import os

def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


#=========Data Preprocessing================

static_folder = os.path.abspath(os.getcwd())
relative_path_file = "/data/xylem_data_useful_minute.csv"

#importing the data
df = pd.read_csv(static_folder+relative_path_file)

df = df.set_index('timestamp')

df = df.drop(['Unnamed: 0'], axis=1)

#checking if time is linear and sorted
# plt.plot(df['time'].values)
# plt.show()


#checking empty columns
emptylist = []

for j in range(df.shape[1]):
    summation = (np.sum(df.iloc[:,-j]))
    if summation == 0:
        emptylist.append(-j)


empty_list_conc = emptylist[-4:]
empty_list_switch = emptylist[0:-4]
#changing to normal index

empty_list_conc = [df.shape[1] + empty_list_conc[j] for j in range(len(empty_list_conc)) ]


#dropping empty conc. columns
df = df.drop(df.columns[empty_list_conc], axis=1)



#df without 0 switch state
df_no_switch = df.drop(df.columns[empty_list_switch],axis=1)


#df with 0/1 switch state
on_off_switches = [-1,-2,-3]
df_no_switch = df_no_switch.drop(df_no_switch.columns[on_off_switches],axis=1)


#removing set points
sp = [10,20]
df_no_switch_sp = df_no_switch.drop(df_no_switch.columns[sp],axis=1)

#removing uncorrelated information (carefully handpicked)
uncorrelated_info = [0,4,6,8,11,13,15,17,20,22,24,32,33,36,34,38,42,44,50,52,53,54,60,66,67]
df_clean_with_impluse  = df_no_switch_sp.drop(df_no_switch_sp.columns[uncorrelated_info],axis=1)

#removing impulse information (carefully handpicked)
impulse_info = [0,1,2,15,17,33,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66]
df_clean  = df_clean_with_impluse.drop(df_clean_with_impluse.columns[impulse_info],axis=1)



columns_names = df_clean.columns.tolist()

#============Training model=============

#Differencing to remove sesonality
data_array = df_clean.values
diff_array = np.diff(data_array,axis=0)



data_train, data_test = split_data(diff_array,0.998)


#Parameters
forecast_days = 14
order = 12
Quantity_interested = 14
maximum_order = 40
minimum_order = 1

test_forecast = data_test[:forecast_days,:]

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

#Model fitting
def model_fitting(d_train, order):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,interest,min_order,max_order):
    mse_list = []
    for i in range(min_order,max_order+1):
        pred = model_fitting(d_train, i)
        # inter_pred = pred[:,interest]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
    ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
    ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
    ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
    #Extracting confidence interval
    
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    ye_80_for, ye_80_for_l, ye_80_for_u = ye_80[0], ye_80[1], ye_80[2]
    ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
    ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2]
    
    fig, ax = plt.subplots()
    x = np.arange(1,15)
    ax.plot(x,ye_95_for[:,Quantity_interested])
    ax.plot(x,test_forecast[:,Quantity_interested])
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
    ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=columns_names[Quantity_interested],b=Quantity_interested))



mse_list = model_selection(data_train,test_forecast, Quantity_interested,minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1




#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    min_mse_order_interest = min_mse_order[Quantity_interested]
    #Manually overide order
    # min_mse_order_interest = 40
    print("Order Choosen {}".format(min_mse_order_interest))
    act_fit_ci(data_train,min_mse_order_interest, Quantity_interested)


###############Checking##############

# calculate the spearmans's correlation between two variables
from numpy.random import randn

from scipy.stats import spearmanr

corr_spearman, _ = spearmanr(data_train,axis = 0)



#Calculating Pearsons correlation

from scipy.stats import pearsonr
# prepare data

# calculate Pearson's correlation

corr_spearman = np.corrcoef(np.transpose(data_train))



# uncorrelated_info = [0,4,6,8,11,13,15,17,20,22,24,32,33,36,34,38,42,44,50,52,53,54,60,66,67]
# df_clean_with_impluse  = df_no_switch_sp.drop(df_no_switch_sp.columns[uncorrelated_info],axis=1)

# for i in range(3):
#     fig, ax = plt.subplots()
#     ax.plot(df_clean.iloc[:,i])


# cl = df_clean.columns
# print(len(cl))








# plot_pacf(data_train[:,1])
# plt.plot(data_train[:,1])

# pacf



# plt.plot(ye_for_l[:,Quantity_interested])
# plt.plot(ye_for_u[:,Quantity_interested])
# plt.plot(test_forecast[:,Quantity_interested])
# plt.title(columns_names[Quantity_interested])


# for j in range(ye_for.shape[1]):
#     plt.plot(ye_for[:,j])
#     plt.plot(test_forecast[:,j])
#     plt.title(columns_names[j])
#     plt.show()




