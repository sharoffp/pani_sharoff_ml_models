#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import datetime
from numpy import array
from numpy import split



from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed


import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

import os



# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test



# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)



def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


#=========Data Preprocessing================

static_folder = os.path.abspath(os.getcwd())
relative_path_file = "/data/xylem_data_useful_minute.csv"

#importing the data
df = pd.read_csv(static_folder+relative_path_file)

df = df.set_index('timestamp')

df = df.drop(['Unnamed: 0'], axis=1)

#checking if time is linear and sorted
# plt.plot(df['time'].values)
# plt.show()


#checking empty columns
emptylist = []

for j in range(df.shape[1]):
    summation = (np.sum(df.iloc[:,-j]))
    if summation == 0:
        emptylist.append(-j)


empty_list_conc = emptylist[-4:]
empty_list_switch = emptylist[0:-4]
#changing to normal index

empty_list_conc = [df.shape[1] + empty_list_conc[j] for j in range(len(empty_list_conc)) ]


#dropping empty conc. columns
df = df.drop(df.columns[empty_list_conc], axis=1)



#df without 0 switch state
df_no_switch = df.drop(df.columns[empty_list_switch],axis=1)


#df with 0/1 switch state
on_off_switches = [-1,-2,-3]
df_no_switch = df_no_switch.drop(df_no_switch.columns[on_off_switches],axis=1)


#removing set points
sp = [10,20]
df_no_switch_sp = df_no_switch.drop(df_no_switch.columns[sp],axis=1)

#removing uncorrelated information (carefully handpicked)
uncorrelated_info = [0,4,6,8,11,13,15,17,20,22,24,32,33,36,34,38,42,44,50,52,53,54,60,66,67]
df_clean_with_impluse  = df_no_switch_sp.drop(df_no_switch_sp.columns[uncorrelated_info],axis=1)

#removing impulse information (carefully handpicked)
impulse_info = [0,1,2,15,17,33,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66]
df_clean  = df_clean_with_impluse.drop(df_clean_with_impluse.columns[impulse_info],axis=1)



columns_names = df_clean.columns.tolist()

#============Training model=============

#Differencing to remove sesonality
data_array = df_clean.values
diff_array = np.diff(data_array,axis=0)



n_input =50
req_parameter = 22
n_output = 1


epochs = 50



data = diff_array
trans = MinMaxScaler()
# trans = StandardScaler()
data = trans.fit_transform(data)
dataset = pd.DataFrame(data)

print(dataset.describe())


# train, test = data[0:1440], data[1440:2880]
#1440  2880  4320  5760  7200
train, val, test = data[0:5760], data[5760:7200], data[7200:]


train_arange = array(split(train, len(train)/1440))

val_arange = array(split(val,1))

test_arange = array(split(test,1))



#Naming

req_parameter = 22
test_x, test_y = to_supervised(test_arange,n_input,req_parameter=22)
val_x, val_y = to_supervised(val_arange,n_input,req_parameter=22)
x,y = to_supervised(train_arange,n_input,req_parameter=22)
train_x = x
# verbose, epochs, batch_size = 1, 20, 10
n_timesteps, n_features, n_outputs = x.shape[1], x.shape[2], y.shape[1]
print("n_output:", n_outputs)
# reshape output into [samples, timesteps, features]
train_y = y.reshape((y.shape[0], y.shape[1], 1))
test_y = test_y.reshape((test_y.shape[0],test_y.shape[1],1))
val_y = val_y.reshape((val_y.shape[0],val_y.shape[1],1))
# define model
model = Sequential()
# model.add(tf.keras.layers.Dense(100,activation='relu'))
# model.add(tf.keras.layers.Dense(50,activation='relu'))
# model.add(tf.keras.layers.Dense(25,activation='relu'))
# # model.add(tf.keras.layers.Dense(10,activation='relu'))
# model.add(tf.keras.layers.Dense(5,activation='relu'))
# # model.add(tf.keras.layers.Dense(2,activation='relu'))
# model.add(tf.keras.layers.Dense(1))

# model.add(LSTM(5, activation='relu', input_shape=(n_timesteps, n_features)))
# model.add(RepeatVector(n_outputs))
# model.add(LSTM(50, activation='relu', return_sequences=True))
# # model.add(LSTM(10, activation='relu', return_sequences=True))
# model.add(LSTM(4, activation='relu', return_sequences=True))
# # model.add(TimeDistributed(Dense(10, activation='relu')))
# # model.add(TimeDistributed(Dense(5, activation='relu')))
# # model.add(TimeDistributed(Dense(5, activation='relu')))
# model.add(Dense(units=1))
# model.add(TimeDistributed(Dense(1)))

model.add(LSTM(200, activation='relu', input_shape=(n_timesteps, n_features)))
model.add(RepeatVector(n_outputs))
model.add(LSTM(200, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(100, activation='relu')))
model.add(TimeDistributed(Dense(1)))



batch_size = len(x)
print("batch_size",batch_size)
verbose = 1

log_dir = "logs/overfit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)




model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(train_y),np.shape(train_x))
model.fit(train_x, train_y, validation_data=(val_x, val_y),epochs=epochs, batch_size=batch_size, verbose=verbose, callbacks=[tensorboard_callback])

ypred = []

ypred_test = []

testing_size = 50

for j in range(30,testing_size):
    x_test = test_x[j,:,:]
    x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))
    yhat_test = model.predict(x_test_reshape, verbose=0)
    ypred_test.append(yhat_test)

ypred_test_a = np.array(ypred_test)
ypred_test_a = ypred_test_a.reshape(-1,1)

plt.plot(test_y[30:testing_size,0])
plt.plot (ypred_test_a[0:testing_size])

