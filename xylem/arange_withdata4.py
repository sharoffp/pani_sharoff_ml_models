
import tensorflow as tf
import numpy as np
import datetime
from numpy import array
from numpy import split
import matplotlib.pyplot as plt
import time

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
from keras.layers import Dropout
from keras.layers import Conv1D
from keras.layers import MaxPooling1D

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler


from data_func import data

# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)

data_train_arange = np.arange(1000)
data_val_arange = np.arange(1000,2000)

data_train_reshaped = data_train_arange.reshape(100,-1)
data_val_reshaped = data_val_arange.reshape(100,-1)


org_data = data()

trans = MinMaxScaler()
# trans = StandardScaler()
org_data = trans.fit_transform(org_data)

org_data = org_data[:7200,:]

org_train = org_data[:5760,:]
org_val = org_data[5760:,:]

# org_train = data_train_reshaped
# org_val = data_val_reshaped

n_input = 60
parameter = 13
n_output = 1

train_arange = array(split(data_train_reshaped, len(data_train_reshaped)/100))
val_arange = array(split(data_val_reshaped, len(data_val_reshaped)/100))

#org
org_train_arange = array(split(org_train,12))
org_val_arange = array(split(org_val,3))


#SL
x,y = to_supervised(train_arange,n_input)
val_x,val_y = to_supervised(val_arange,n_input)


org_x, org_y = to_supervised(org_train_arange,n_input,req_parameter=parameter)
org_val_x, org_val_y = to_supervised(org_val_arange,n_input,req_parameter=parameter)



#Naming
train_x = x

org_train_x = org_x

org_n_timesteps, org_n_features, org_n_outputs = org_train_x.shape[1], org_train_x.shape[2], org_y.shape[1]


print("n_output:", org_n_outputs)

org_train_y = org_y.reshape((org_y.shape[0], org_y.shape[1], 1))
org_val_y = org_val_y.reshape((org_val_y.shape[0], org_val_y.shape[1],1))


start_time = time.time()

# define model
# model = Sequential()
# model.add(LSTM(200, activation='relu', input_shape=(org_n_timesteps, org_n_features)))
# model.add(RepeatVector(org_n_outputs))
# model.add(LSTM(200, activation='relu', return_sequences=True))
# model.add(TimeDistributed(Dense(100, activation='relu')))
# model.add(TimeDistributed(Dense(1)))


# # define model l=== original ====
# model = Sequential()
# model.add(LSTM(10, activation='relu', input_shape=(org_n_timesteps,org_n_features)))
# model.add(RepeatVector(org_n_outputs))
# model.add(LSTM(10, activation='relu', return_sequences=True))
# model.add(TimeDistributed(Dense(10, activation='relu')))
# # model.add(TimeDistributed(Dense(5, activation='relu')))
# model.add(TimeDistributed(Dense(1)))

from keras.layers import Flatten

model = Sequential()
model.add(LSTM(64, return_sequences=True, input_shape=(org_n_timesteps,org_n_features)))
model.add(LSTM(128, return_sequences=True))
model.add(LSTM(256, return_sequences=True))
model.add(LSTM(128, return_sequences=True))
model.add(LSTM(64, return_sequences=True))
model.add(TimeDistributed(Dense(1)))


epochs = 1000
batch_size = len(org_train_x)
print("batch_size",batch_size)
verbose = 1

log_dir = "logs/trial/stack_overflow/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)




model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(org_train_y),np.shape(org_train_x))

print("Shape of val_y & val_x:",np.shape(org_val_y),np.shape(org_val_x))

model.fit(org_train_x, org_train_y,validation_data=(org_val_x, org_val_y), epochs=epochs, batch_size=batch_size, verbose=verbose, callbacks=[tensorboard_callback])


training_time = start_time - time.time()
print("Total time taken", training_time)

org_pred = []

for i in range(0,21):
    x_test = org_val_x[i,:,:]
    x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))
    yhat_arange = model.predict(x_test_reshape, verbose=0)
    # ytest_arange = org_val_y[j]
    org_pred.append(yhat_arange)

org_pred = np.array(org_pred)
org_pred = org_pred.reshape(-1,1)

plt.plot(org_pred[:20],label="pred")
plt.plot(org_val_y[:20,0,0], label = "true")
plt.title("val")
plt.legend()
plt.show()



train_pred = []
x
for i in range(0,21):
    x_train_test = org_train_x[i,:,:]
    x_train_test_reshape = x_train_test.reshape((1,x_train_test.shape[0],x_train_test.shape[1]))
    yhat_train_arange = model.predict(x_train_test_reshape, verbose=0)
    # ytest_arange = org_val_y[j]
    train_pred.append(yhat_train_arange)

train_pred = np.array(train_pred)
train_pred = train_pred.reshape(-1,1)

plt.plot(train_pred[:20],label="pred")
plt.plot(org_train_y[:20,0,0], label = "true")
plt.title("train")
plt.legend()
plt.show()

