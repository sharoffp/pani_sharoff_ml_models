
# multivariate multi-step encoder-decoder lstm
from math import sqrt
from numpy import split
from numpy import array
from pandas import read_csv
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed

import matplotlib.pyplot as plt

import numpy as np

import datetime
import tensorflow as tf




import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

import os

def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


#=========Data Preprocessing================

static_folder = os.path.abspath(os.getcwd())
relative_path_file = "/data/xylem_data_useful_minute.csv"

#importing the data
df = pd.read_csv(static_folder+relative_path_file)

df = df.set_index('timestamp')

df = df.drop(['Unnamed: 0'], axis=1)

#checking if time is linear and sorted
# plt.plot(df['time'].values)
# plt.show()


#checking empty columns
emptylist = []

for j in range(df.shape[1]):
    summation = (np.sum(df.iloc[:,-j]))
    if summation == 0:
        emptylist.append(-j)


empty_list_conc = emptylist[-4:]
empty_list_switch = emptylist[0:-4]
#changing to normal index

empty_list_conc = [df.shape[1] + empty_list_conc[j] for j in range(len(empty_list_conc)) ]


#dropping empty conc. columns
df = df.drop(df.columns[empty_list_conc], axis=1)



#df without 0 switch state
df_no_switch = df.drop(df.columns[empty_list_switch],axis=1)


#df with 0/1 switch state
on_off_switches = [-1,-2,-3]
df_no_switch = df_no_switch.drop(df_no_switch.columns[on_off_switches],axis=1)


#removing set points
sp = [10,20]
df_no_switch_sp = df_no_switch.drop(df_no_switch.columns[sp],axis=1)

#removing uncorrelated information (carefully handpicked)
uncorrelated_info = [0,4,6,8,11,13,15,17,20,22,24,32,33,36,34,38,42,44,50,52,53,54,60,66,67]
df_clean_with_impluse  = df_no_switch_sp.drop(df_no_switch_sp.columns[uncorrelated_info],axis=1)

#removing impulse information (carefully handpicked)
impulse_info = [0,1,2,15,17,33,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66]
df_clean  = df_clean_with_impluse.drop(df_clean_with_impluse.columns[impulse_info],axis=1)



columns_names = df_clean.columns.tolist()

#============Training model=============

#Differencing to remove sesonality
data_array = df_clean.values
diff_array = np.diff(data_array,axis=0)




# split a univariate dataset into train/test sets
def split_dataset(data):
    # split into standard weeks
    train, test = data[0:1440], data[1440:2880]
    # restructure into windows of weekly data
    print(np.shape(train), np.shape(test))
    train = array(split(train, len(train)/1440))
    print("length:", len(train), "shape_of_train", np.shape(train))
# 	test = array(split(test, len(test)/1440))
    test = array(split(test, 1))
    return train, test

# evaluate one or more weekly forecasts against expected values
def evaluate_forecasts(actual, predicted):
	scores = list()
	# calculate an RMSE score for each day
	for i in range(actual.shape[1]):
		# calculate mse
		mse = mean_squared_error(actual[:, i], predicted[:, i])
		# calculate rmse
		rmse = sqrt(mse)
		# store
		scores.append(rmse)
	# calculate overall RMSE
	s = 0
	for row in range(actual.shape[0]):
		for col in range(actual.shape[1]):
			s += (actual[row, col] - predicted[row, col])**2
	score = sqrt(s / (actual.shape[0] * actual.shape[1]))
	return score, scores

# summarize scores
def summarize_scores(name, score, scores):
	s_scores = ', '.join(['%.1f' % s for s in scores])
	print('%s: [%.3f] %s' % (name, score, s_scores))

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=14,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)




# load the new file
# dataset = read_csv('household_power_consumption_days.csv', header=0, infer_datetime_format=True, parse_dates=['datetime'], index_col=['datetime'])
# split into train and test
train, test = split_dataset(diff_array)
# evaluate model and get scores
n_input = 480
n_out = 2
req_parameter = 22
train_x, train_y = to_supervised(train,n_input,n_out,req_parameter)

# pred, test_pred = evaluate_model(train, test, n_input)


# evaluate a single model
	# fit model
# model = build_model(train, n_input)



# train the model
# def build_model(train, n_input):
	# prepare data
# train_x, train_y = to_supervised(train, n_input)
# define parameters
verbose, epochs, batch_size = 1, 2, 16
n_timesteps, n_features, n_outputs = train_x.shape[1], train_x.shape[2], train_y.shape[1]
print("n_output:", n_outputs)
# reshape output into [samples, timesteps, features]
train_y = train_y.reshape((train_y.shape[0], train_y.shape[1], 1))
# define model
model = Sequential()
model.add(LSTM(10, activation='relu', input_shape=(n_timesteps, n_features)))
model.add(RepeatVector(n_outputs))
model.add(LSTM(10, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(10, activation='relu')))
# model.add(TimeDistributed(Dense(5, activation='relu')))
model.add(TimeDistributed(Dense(1)))

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(train_y),np.shape(train_x))
model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, callbacks=[tensorboard_callback])



	# history is a list of weekly data
history = [x for x in train]
# walk-forward validation over each week
predictions = list()
for i in range(len(test)):
    # predict the week

    data = array(history)
    data = data.reshape((data.shape[0]*data.shape[1], data.shape[2]))
    # retrieve last observations for input data
    input_x = data[-n_input:, :]
    # reshape into [1, n_input, n]
    input_x = input_x.reshape((1, input_x.shape[0], input_x.shape[1]))
    # forecast the next week
    yhat = model.predict(input_x, verbose=0)
    # we only want the vector forecast
    yhat = yhat[0]
    print(yhat)
    # store the predictions
    predictions.append(yhat)
    # get real observation and add to history for predicting the next week
    history.append(test[i, :])
    # evaluate predictions days for each week
predictions = array(predictions)

test_pred = test[:, :, req_parameter]
pred_reshape = predictions.reshape(-1,n_out)



plt.plot(pred_reshape[0,:])
plt.plot(test_pred[0,0:n_out])
plt.show()



train_te = train_x[0,:,:]
train_te_res = train_te.reshape((1, train_te.shape[0], train_te.shape[1]))
yhat_train_te = model.predict(train_te_res,verbose=0)

yhat_train_te = yhat_train_te.reshape(n_out,-1)

train_te_y= train_y[0,:,:]
train_te_y=train_te_y.reshape(n_out,-1)

plt.plot(yhat_train_te,label="yhat")
plt.plot(train_te_y,label="test")
plt.show()
# for j in range(0,pred_reshape.shape[1]):
#     plt.plot(pred_reshape[j,:])
#     plt.plot(test_pred[j,:])
#     plt.show()


print("yhat: ",yhat_train_te)
print("test: ",train_te_y)
# plt.plot
