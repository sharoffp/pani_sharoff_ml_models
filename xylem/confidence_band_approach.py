
#libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


#loading the data


df = pd.read_csv("xylem-2020-09-28_parsed.csv")

df_1 = df.iloc[1,:]
not_na_df_1 = df_1.notna()

#non zeros columns in 

index_non_zeros = np.flatnonzero(not_na_df_1) 


#datetime is in minute:second.millisecond

#Sorting by datetime

df_sort_datetime = df.sort_values(by=['datetime'])

type_np_float = type(df_sort_datetime.iloc[1,2])


# plot_list = []
# for i in range(df_sort_datetime.shape[1]): 
#     if type(df_sort_datetime.iloc[1,i]) == type_np_float:
#         fig, ax = plt.subplots()
#         temp =  df_sort_datetime.iloc[:,i]
#         temp = temp.reset_index(drop="True")
#         ax.plot(temp)
#         plot_list.append(i)
#         print(i)


df_sort_array = df_sort_datetime.to_numpy()

df_time = pd.read_csv("xylem-2020-09-28_parsed_timestamp.csv")
# df.iloc[:,138]

new_df = pd.read_csv("new_df.csv")

new_df = new_df.drop(columns=['Unnamed: 0'])

cols = new_df.columns.tolist()

cols = cols[-1:] + cols[:-1]

new_df = new_df[cols]

plt.plot(new_df['timestamp'],new_df['Blower_Header_Pressure_(psi)'],'.k')


timestamp = new_df['timestamp'].values
dc_bus_voltage = new_df['DC_bus_voltages']

dc_bv = dc_bus_voltage.interpolate(method='spline', order=2)


dc_bv = dc_bus_voltage.interpolate()

plt.plot(timestamp,dc_bus_voltage)
plt.plot(timestamp,dc_bv)


nans_count = []


#=============


for i in range(new_df.shape[1]):    
    col_val = new_df.iloc[:,i]
    count = col_val.isna().sum()
    nans_count.append(count)


nans_count = np.array(nans_count)

nans_count_l_80k = nans_count < 80000

cols_ar = np.array(cols)

print(cols_ar[nans_count_l_80k])

plt.plot(nans_count)
plt.xlabel("dataframe column")
plt.ylabel("no. of missing values")


# dc_bus_voltage.isna().sum()


# dc_bus_voltage



#DC bus voltage 91693/91839
#We have only 3 values that is less than 80k
#

#==================Ad Cleaned dataset=============

ad_x_df = pd.read_excel("xylem-data-useful.xlsx")
ad_ind = ad_x_df['Unnamed: 0'].values
ts_ad = new_df['timestamp'][ad_ind]

ts_ad_ar = ts_ad.values

ad_x_df2 = ad_x_df

ad_x_df2['time'] = ts_ad_ar


#reording time column
cols_ad = ad_x_df2.columns.tolist()


cols_ad = ad_x_df2.columns.tolist()
cols_ad = cols_ad[-1:] + cols_ad[:-1]
ad_x_df2_ad = ad_x_df2[cols_ad]


def change_col_last_to_first(ad_x_df2_ad):
    cols_ad = ad_x_df2_ad.columns.tolist()
    cols_ad = cols_ad[-1:] + cols_ad[:-1]
    ad_x_df2_ad = ad_x_df2[cols_ad]
    return ad_x_df2_ad
# ad_x_df2_ad.to_csv("xylem_data_useful_with_timestamp.csv")
# ad_x_df2_ad.to_excel("xylem_data_useful_with_timestamp_excel.xlsx")


times = ad_x_df2_ad['time']
times_sort = times.sort_values()
times_diff = np.diff(times_sort.values)

plt.hist(times_diff)

# from hist plot 50000 time intervals`

tt = times
tspd = pd.Timestamp(tt[1])


tspd_ts = pd.to_datetime(tt, unit='ms')


ad_x_df2_ad['timestamp'] = pd.to_datetime(ad_x_df2_ad['time'],unit='ms')
ad_x_df2_ad = change_col_last_to_first(ad_x_df2_ad)


#reording timestamp column
cols_ad = ad_x_df2_ad.columns.tolist()


cols_ad = cols_ad[-1:] + cols_ad[:-1]
ad_x_df2_ad = ad_x_df2_ad[cols_ad]

#safe copy
ad_x_df3_ad = ad_x_df2_ad


#setting timestamp as index


ad_x_df3_ad = ad_x_df3_ad.set_index('timestamp')


ad_x_df3_ad = ad_x_df3_ad.sort_index(axis = 0)


ad_x_df3_ad = ad_x_df3_ad.fillna(method='bfill')

# ad_x_df3_ad = ad_x_df3_ad.interpolate(method='cubic')

ad_x_df3_ad_resampled =ad_x_df3_ad.resample('T').mean()


#resampled safeversion
# ad_x_df3_ad_resampled = ad_x_df3_ad

print(ad_x_df3_ad.isnull().sum().sum())

print(ad_x_df3_ad_resampled.isnull().sum().sum())


null_index_ad_x_df3_ad_resamp =pd.isnull(ad_x_df3_ad_resampled).any(1).nonzero()[0]
print(null_index_ad_x_df3_ad_resamp)

null_index_ad_x_df3_ad =pd.isnull(ad_x_df3_ad).any(1).nonzero()[0]
print(null_index_ad_x_df3_ad)


#safeversion
min_df = ad_x_df3_ad_resampled

min_df= min_df.interpolate(method='cubic')

print(min_df.isnull().sum().sum())

ind_missing = [2992, 2993, 2996, 2998, 2999, 3000, 3003, 3004, 3007, 4213, 4214]

# min_df.to_csv("xylem_data_useful_minute.csv")
# min_df.to_excel("xylem_data_useful_minute.xlsx")

