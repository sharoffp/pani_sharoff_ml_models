
import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval
from scipy.ndimage.filters import uniform_filter1d


#3 days
def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n


#Calculating running mean
def running_mean(x, N):
    rm = []
    for i in range(x.shape[1]):
        rm_i = uniform_filter1d(x[:,i], size=N)
        rm.append(rm_i)
    return np.transpose(np.array(rm))


df_csv = pd.read_csv("./data/pani_6month.csv")


df1 = df_csv.drop([0,1,2], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)
df1 = df1.fillna(method='ffill')


title  = df_csv.iloc[0,:]
title = np.array(title)
title = title[1:]

# df1 = df1.drop([0],axis=0)
df_array = df1.to_numpy()
df_array_hr = df_array

# df_array_hr = df_array
df_array_hr = np.array(df_array_hr, dtype=np.float)


N = 30
run_mean = running_mean(df_array_hr,N)


#Differencing to avoid seasonality
diff_array = np.diff(run_mean,axis=0)


cut_off_range = len(df_array_hr)



#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)



# plot_pacf(df_array[:,14],lags=70)

#Parameters
forecasting_days = 60
order = 50

title_list = title.tolist()


#Model fitting
def model_fitting(d_train, order,forecast_days = 14):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,min_order,max_order,forecast_days=14):
    pred_list = []
    # d_test = d_test[:forecast_days,interest]
    for i in range(min_order,max_order+1):
        print(i)
        pred = model_fitting(d_train, i, forecast_days)
        # pred = pred[:,interest]
        # mse = m_s_e(d_test,pred)
        # mse_list.append(mse)
        print("order {} completed".format(i))
        pred_list.append(pred)
    # min_mse_order = np.argmax(mse_list)+1
    return pred_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))
    model_fit = model.fit(min_mse_order_interest)
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
 #   ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
 #   ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
  #  ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
    #Extracting confidence interval
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]

 #   ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
  #  ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2
    fig, ax = plt.subplots()
    x = np.arange(1,15)
    ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
    ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
    # ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    # ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    # ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
    ax.legend()
    # fig.savefig("./plots/mins/{}.pdf".format(title[Quantity_interested]))






def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested, hard_title_index, forecast_days=14):
    model = VAR((data_train))
    model_fit = model.fit(min_mse_order_interest)
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
 #   ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
 #   ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
  #  ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
    #Extracting confidence interval
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    
    initial_value = np.reshape(df_array_hr[0,:],(1,-1))
    conc_data_train = np.concatenate((initial_value,data_train), axis=0)
    inv_data_train = np.cumsum(conc_data_train, axis = 0)
    
    init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
    conc_data_forecast = np.concatenate((init_forecast,ye_95_for), axis=0)
    conc_data_fore_ciu = np.concatenate((init_forecast,ye_95_for_u), axis=0)
    conc_data_fore_cil = np.concatenate((init_forecast,ye_95_for_l), axis=0)
    conc_data_test = np.concatenate((init_forecast,data_test), axis=0)
    
    
    inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
    inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
    inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
    inv_test_data = np.cumsum(conc_data_test, axis = 0)
    
    
 #   ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
  #  ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2
    print(np.shape(inv_forecast[:forecast_days, Quantity_interested]))
    fig, ax = plt.subplots()
    x = np.arange(1,forecast_days+1)
    print(np.shape(x))
    ax.plot(x,inv_forecast[:forecast_days,Quantity_interested], color='turquoise',label="pred")
    ax.plot(x,inv_test_data[:forecast_days,Quantity_interested], color = 'k', label="true")
    ax.fill_between(x,inv_forecast_l[:forecast_days,Quantity_interested], inv_forecast_u[:forecast_days,Quantity_interested], color='b', alpha=.1)
    # ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    # ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    # ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=hard_title[hard_title_index],b=Quantity_interested))
    ax.legend()
    # fig.savefig("./plots/invdata_min/{}.pdf".format(title[Quantity_interested]))





#HARD_coded title;
hard_title = ["RO022 FIT-202 Product Flow PV", 
"RO022 PT-203 1st Stage Inlet Pressure PV",
"RO022 PT-204 2nd Stage Inlet Pressure PV",
"RO022 PT-205 3rd Stage Inlet Pressure PV",
"RO021 FIT-202 Product Flow PV",
"RO021 PT-203 1st Stage Inlet Pressure PV",
"RO021 PT-204 2nd Stage Inlet Pressure PV",
"RO021 PT-205 3rd Stage Inlet Pressure PV",
"FIT-202 Product Flow PV",
"PT-203 1st Stage Inlet Pressure PV",
"PT-204 2nd Stage Inlet Pressure PV",
"PT-205 3rd Stage Inlet Pressure PV",
"FIT-202 Product Flow PV",
"PT-203 1st Stage Inlet Pressure PV",
"PT-204 2nd Stage Inlet Pressure PV",
"PT-205 3rd Stage Inlet Pressure PV",
"RO Feed ORP",
"RO Feed Conductivity",
"RO Feed Temperature"]


minimum_order = 1
maximum_order = 3
# Quantity_interested = 33


# mse_list = model_selection(data_train, data_test, minimum_order,maximum_order)
# min_mse_order = np.argmin(mse_list,axis=0)+1

# mse_dictionary = {"mse":mse_list, "min_mse":min_mse_order}
# mse_d = pickle.load(open("save_mse_min_1__max_20_6months.p","rb"))
# minimum_mse_ord = mse_d['min_mse']


mse_dict = {}
min_mse_dict = {}



required_list = [3213,3218,3219,3220,3113,3118,3119,3120,101213,101218,101219,101220,101113,101118,101119,101120,100937,100938,100939]

index_req_list = [title_list.index(str(item)) for item in required_list]



pred_list = model_selection(data_train, data_test,minimum_order,maximum_order,forecasting_days)

pred_list_tensor = np.array(pred_list)


def test_mse(d_test, min_order, max_order, interest, forecast_days = 14):
    mse_list = []
    d_test = d_test[:forecast_days,interest]
    for k in range(min_order-1, max_order): #one number less for array indexing
        pred = pred_list_tensor[k,:,interest]
        # pred = pred[:forecast_days,interest]
        mse = m_s_e(d_test,pred)
        # print(mse)
        mse_list.append(mse)
    return mse_list


#Parameters
for kk in index_req_list:    
    Quantity_interested = kk
    print(title[kk])
    mse_list =   test_mse(data_test, minimum_order, maximum_order,Quantity_interested, forecasting_days)
    mse_dict["{}".format(kk)] = mse_list
    min_mse_order = np.argmin(mse_list,axis=0)+1
    print("the minimum mse oreder for {} is {}".format(kk, min_mse_order))
    min_mse_dict["{}".format(kk)] = min_mse_order
    print("Order Choosen {} for {}".format(min_mse_order,title[kk]))

description = ["description: ", "mse_dict", "min_mse_order"]
mse_and_min_order = [mse_dict, min_mse_dict, description]

import pickle
pickle.dump( mse_and_min_order, open("save_mse_in_min_for_1hr_0_50_6months.p", "wb"))



