#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

import tensorflow as tf
import numpy as np
import datetime
from numpy import array
from numpy import split
import matplotlib.pyplot as plt
import time

from numpy import array
from numpy import split

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
from keras.layers import Dropout
from keras.layers import Conv1D
from keras.layers import MaxPooling1D

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

from keras.layers import Flatten

# from data_func import data

#3 days
def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

#Parameters
forecast_days = 24
order = 72

folder_path = "./data/metrics/"
mypath = folder_path

from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

onlyfiles = onlyfiles[1:]

df_csv = pd.read_csv("./data/engineering_preprocessed/output_merge_cor_order_sort_date.csv")

title = df_csv.columns.tolist()
datetime = df_csv['datetime']

# df_csv = df_csv.drop(columns=['datetime'])
df_csv = df_csv.set_index(['datetime'])
df_csv.columns = onlyfiles
df_csv.to_csv('d_date_time_sort_with_index',sep=',')

df_csv = df_csv.fillna(method='bfill')

df_array = df_csv.to_numpy()
df_array_hr = df_array[range(0,len(df_array),60),:]

diff_array = np.diff(df_array_hr,axis=0)

cut_off_range = len(df_array_hr)
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

ratio = (len(df_ar_no_out)-forecast_days)/len(df_ar_no_out)

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out, ratio =ratio)

# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)


org_train = data_train
org_test = data_test

n_input = 48
parameter = 0
n_output = forecast_days


#org
org_train_arange = np.array(np.split(org_train,1))
org_val_arange = np.array(np.split(org_test,1))

org_x, org_y = to_supervised(org_train_arange,n_input,n_out=n_output,req_parameter=parameter)
org_val_x, org_val_y = to_supervised(org_val_arange,n_input,n_out=n_output,req_parameter=parameter)



#Naming
org_train_x = org_x
org_n_timesteps, org_n_features, org_n_outputs = org_train_x.shape[1], org_train_x.shape[2], org_y.shape[1]


print("n_output:", org_n_outputs)
org_train_y = org_y.reshape((org_y.shape[0], org_y.shape[1], 1))

start_time = time.time()

model = Sequential()
model.add(LSTM(10, activation='relu', input_shape=(org_n_timesteps,org_n_features)))
model.add(RepeatVector(org_n_outputs))
model.add(LSTM(10, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(9, activation='relu')))
model.add(TimeDistributed(Dense(5, activation='relu')))
model.add(TimeDistributed(Dense(1)))

epochs = 200
batch_size = len(org_train_x)
print("batch_size",batch_size)
verbose = 1

model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(org_train_y),np.shape(org_train_x))

print("Shape of val_y & val_x:",np.shape(org_val_y),np.shape(org_val_x))

model.fit(org_train_x, org_train_y, epochs=epochs, batch_size=batch_size, verbose=verbose)

#Model fitting
def model_fitting(d_train, order,forecast_days = 14):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]

#Model selection
def model_selection(d_train,d_test,min_order,max_order,forecast_days=14):
    mse_list = []
    for i in range(min_order,max_order+1):
        print(i)
        pred = model_fitting(d_train, i)
        # pred = pred[:,interest]
        d_test = d_test[:forecast_days,:]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    fig, ax = plt.subplots()
    x = np.arange(1,25)
    ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
    ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
    ax.legend()
    fig.savefig("./plots/new_preprocessed_data/{}.pdf".format(title[Quantity_interested]))

minimum_order = 1
maximum_order = 40
Quantity_interested = 33


mse_list = model_selection(data_train, data_test, minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1
min_mse = np.min(mse_list,axis=0)
min_rmse = np.sqrt(min_mse)


mse_dictionary = {"mse":mse_list, "min_mse":min_mse_order}


import pickle
pickle.dump( mse_dictionary, open( "save_mse_min_01__max_40_6months_new_data.p", "wb" ) )

mse_dict = {}
min_mse_list = []

#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    min_order_for_Quantity = min_mse_order[kk]
    act_fit_ci(data_train,min_order_for_Quantity, Quantity_interested,forecast_days=24)
    print("Order Choosen {} for {}".format(min_order_for_Quantity,title[kk]))