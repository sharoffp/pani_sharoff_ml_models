#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

df = pd.read_excel("./data/Pani_Export_All_2_Weeks_0828to0911_2020.xls")


df1 = df.drop([0,1], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)

title  = df.iloc[0,:]
title = np.array(title)
title = title[1:]
df_array = df1.values


#=============Ploting=================

# for i in range(0, df_array.shape[1]):
#     fig, ax = plt.subplots()
#     ax.plot(df_array[:,i])
#     ax.set_title("{}".format(title[i]))
#     ax.set_xlabel("Time steps")
#     ax.set_ylabel("Value")

#=============Evaluating the no. of missing values===========
list_nan = []
for i in range(0,df_array.shape[1]):
    temp = df_array[:,i]
    list_nan.append(pd.isnull(temp).sum())

#Loading alerts ecolabs
alerts_df = pd.read_excel("ecolab_alert_list.xlsx")




#Separating different thresholds
tags = alerts_df["Tag"].values
low_critical = alerts_df["Low Critical"]



match = []
# match_index = {}
match_in =[]
# for i in range(len(Tags)):
sub = "Outlet pressure"
    # s = title
for i in range(len(title)):
    str_temp = title[i]
    if sub.lower() in str_temp.lower():
        print(1)
        match.append(str_temp)
        match_in.append(i)

match_title = title[match_in]

    
#Loading Maping
mapping_df = pd.read_excel("2-week-map.xlsx")
map_array = mapping_df.values
map_title = mapping_df.iloc[:,0].values
map_title_li = list(map_title)

map_dict = {}


# low_critical = np.empty(len(#alert_string))
# alerts_high_information = np.empty(len)

total_alerts = np.empty((len(tags),6))
# total_alerts_high_critical = np.empty(len(#alert_strings))
# total_alerts_high_informational =  np.empty(len(#alert_strings))
# total_alerts_low_informational =  np.empty(len(#alert_strings))
# total_alerts_high_warning = np.empty(len(#alert_strings))
# total_alerts_low_warning = np.empty(len(#alert_strings))

low_critical =  alerts_df.iloc[:,:]["Low Critical"]
high_critical =  alerts_df.iloc[:,:]["High Critical"]
low_informational =  alerts_df.iloc[:,:]["Low Informational"]
high_informational =  alerts_df.iloc[:,:]["High Informational"]
low_warning = alerts_df.iloc[:,:]["Low Warning"]
high_warning = alerts_df.iloc[:,:]["High Warning"]

alerts_threshold = [low_critical,high_critical,low_informational,high_informational,low_warning,high_warning]

alerts = np.empty((len(tags),21599,6))

match_list = []
match_list_names = []

data_value = np.empty((21599,len(tags)))

for i in range(len(tags)):
    # print(1)
    match_status = (tags[i]) in map_title
    if match_status == True:
        match_list.append(i)
        match_list_names.append(tags[i])
        # print(tags[i])
        # print(i)
        index_match = map_title_li.index(tags[i])                                
        req_values = df_array[:,index_match]
        data_value[:,i] = req_values
        for j in range(6):
            # print(2)
            threshold = alerts_threshold[j][i]
            if threshold is not "-":
                if j % 2 == 0:
                    alerts[i,:,j] = req_values < threshold
                else:
                    alerts[i,:,j] = req_values > threshold
            else:
                alerts_threshold[j][i] = np.nan
                alerts[i,:,j] = np.nan


alerts_detected = alerts[match_list,:,:]
ml = [6]
i = 0

#Generating Plots
for j,i in zip(match_list,match_list_names):    
    fig, ax = plt.subplots()
    ax.plot(data_value[:,j], 'k')
    ax.axhline(y=alerts_threshold[0][j], color='r', linestyle = 'dashed')
    ax.axhline(y=alerts_threshold[1][j], color='r')
    ax.axhline(y=alerts_threshold[2][j], color='y', linestyle = 'dashed')
    ax.axhline(y=alerts_threshold[3][j], color='y')
    ax.axhline(y=alerts_threshold[4][j], color='b', linestyle = 'dashed')
    ax.axhline(y=alerts_threshold[5][j], color='b')
    plt.title("{}".format(i))    
    plt.xlabel("Time steps")
    plt.ylabel("Values")
    # plt.text(0.02, 0.5, textstr, fontsize=14, transform=plt.gcf().transFigure)
    # plt.savefig("{}.pdf".format(i))
    # i+=1    
    

#Generating Legends for the generated
plt.plot([], linestyle="dashed", color="r", label="Low Critical")
plt.plot([], color="r", label="High Critical")
plt.plot([], linestyle="dashed", color="b", label="Low Informational")
plt.plot([], color="b", label="High Informational")
plt.plot([], linestyle="dashed", color="y", label="Low Warning")
plt.plot([], color="y", label="High Warning")
plt.plot([],color = "k", label = "values")
plt.legend()# handles = [colors]
plt.axis('off')
# plt.savefig('legends.pdf')


#Counting the no. of alerts for each sensors

label_count = ["low critical", "high critical", "low informational", "high informational", "low warning", "high warning"]
counts = []
counts.append(label_count)
for i in match_list:
    alert_count_one = []
    for j in range(len(label_count)):
        alert_count_one.append(np.sum(alerts[i,:,j]))
    counts.append(alert_count_one)

ca = np.array(counts)
# counts_alerts = np.empty((alerts[np.6,:,0]))


match_list_ar = np.array(match_list_names)
# pd.DataFrame(match_list_ar).to_csv("counts_names.csv")


##Plotting Histogram

min_per_day = 1440
l = 0
r = min_per_day

low_critical_count_per_day = []
high_critical_count_per_day = []
low_informational_count_per_day = []
high_informational_count_per_day = []
low_warning_count_per_day = []
high_warning_count_per_day = []

for i in range(math.ceil(data_value.shape[0]/min_per_day)+1):
    # print(match_list_names[0])
    low_critical_count_per_day.append(np.sum(alerts_detected[:,l:r,0]))
    # print(low_critical_count_per_day)
    high_critical_count_per_day.append(np.sum(alerts_detected[:,l:r,1]))
    low_informational_count_per_day.append(np.sum(alerts_detected[:,l:r,2]))
    high_informational_count_per_day.append(np.sum(alerts_detected[:,l:r,3]))
    low_warning_count_per_day.append(np.sum(alerts_detected[:,l:r,4]))
    high_warning_count_per_day.append(np.sum(alerts_detected[:,l:r,5]))
    # if r <= (data_value.shape[0]):
    l += 1440 
    r += 1440    
    # print(l,r)
        
lccpd = np.array(low_critical_count_per_day)
hccpd = np.array(high_critical_count_per_day)
licpd = np.array(low_informational_count_per_day)
hicpd = np.array(high_informational_count_per_day)
lwcpd = np.array(low_warning_count_per_day)
hwcpd = np.array(high_warning_count_per_day)    


###Histogram

# plt.hist(lccpd,density=True,stacked=True)
# plt.hist(lccpd)
# plt.hist(hccpd)
# plt.hist(licpd)
# plt.hist(hicpd)
# plt.hist(lwcpd)
# plt.hist(hwcpd)    
    

###Prob. Histogram

results, edges = np.histogram(hwcpd[:-1], normed=True)
binWidth = edges[1] - edges[0]
plt.bar(edges[:-1], results*binWidth, binWidth)
plt.title("High Warning")
# plt.savefig("high_warning_hist.pdf")
# el  = []    
# for i in range(15):
#     el.append(np.sum(alerts_detected[0,(i*1440):(1439+i*1440)+1,0]))
#     print(i*1440,1439+i*1440)




#conductivity threshold measure

no_of_triggers = 3

h = [1,2,3,4,5,66,78,88,99]

h = df_array[:,5]

hr = np.arange(min(h),max(h))
counts, bin_edges = np.histogram(h, bins = len(hr))
counts_rev = counts[::-1]
plt.bar(hr,counts)
# counts_array = np.array(counts)
counts_cs = counts_rev.cumsum()
bool_count = counts_cs > no_of_triggers
bool_count_true = np.where(bool_count)[0]
threshold = max(h) - bool_count_true[0]
print(threshold)


hs =  np.sort(h)
hs[100]
hs[-100]
plt.plot(h)
plt.axvline(x=hs[100],color="r",linestyle="dashed")
plt.axvline(x=hs[-100],color="r")


def lower_limit(data,no_of_triggers = 100):
    bin_range = np.arange(min(data),max(data)+1)
    counts, bin_edges = np.histogram(data,bins = len(bin_range))
    counts_array = np.array(counts)
    counts_cs = counts_array.cumsum()
    bool_count = counts_cs > no_of_triggers
    bool_count_true = np.where(bool_count)[0]
    threshold = bool_count_true[0]
    return threshold

def higher_limit(data,no_of_triggers = 100):
    # h = [1,2,3,4,5,66,78,88,99]
    hr = np.arange(min(data),max(data))
    counts, bin_edges = np.histogram(data, bins = len(hr))
    counts_rev = counts[::-1]
# counts_array = np.array(counts)
    counts_cs = counts_rev.cumsum()
    bool_count = counts_cs > no_of_triggers
    bool_count_true = np.where(bool_count)[0]
    threshold = max(h) - bool_count_true[0]
    plt.bar(hr,counts)
    plt.axvline(x=threshold,color='r')
    print(threshold)



def find_limit(data,no_of_triggers = 100):
    bin_range = np.arange(min(data),max(data)+1)
    counts, bin_edges = np.histogram(data,bins = len(bin_range))
    counts_array = np.array(counts)
    # if type_limit=="lower":
    counts_cs_l = counts_array.cumsum()
    bool_count_l = counts_cs_l > no_of_triggers
    bool_count_true_l = np.where(bool_count_l)[0]
    threshold_l = bool_count_true_l[0]
    # plt.bar(data)
    # elif type_limit=="higher":
    counts_rev_h = counts[::-1]
# counts_array = np.array(counts)
    counts_cs_h = counts_rev_h.cumsum()
    bool_count_h = counts_cs_h > no_of_triggers
    bool_count_true_h = np.where(bool_count_h)[0]
    threshold_h = max(data) - bool_count_true_h[0]
    plt.bar(bin_range,counts)
    plt.axvline(x=threshold_l,color='r',linestyle='dashed',label="lower limit - no. of trigs {t} is.{l}".format(l=threshold_l,t=no_of_triggers))
    plt.axvline(x=threshold_h,color='r',label="higher limit -no. of triggers{t} is {h}".format(h=threshold_h,t=no_of_triggers))
    plt.legend()
    # plt.
    # else:
    # print("specify the type")
    return threshold_l, threshold_h

df_nona = df1

df_nona = df_nona.fillna(method='ffill')

dfa_nona_array = df_nona.values

dfa_nona_array = dfa_nona_array.astype(float)

dfa5_nona = dfa_nona_array[:,5]

find_limit(df_array[:,5])


match_list_names

def offon(data):
    data_int = np.array(data)
    data_int = data_int.astype(int)
    offtoon = []
    offtoon.append(0) #Initial state
    if data_int[0] == np.nan:
        data_int[0] = 0
    for i in range(1,len(data_int)):
        if data_int[i] == np.nan:
            data_int[i] = data_int[i-1]
        diff = (data_int[i]-data_int[i-1])
        if diff == 1:
            offtoon.append(1)
        elif diff ==0:
            offtoon.append(0)
        else:
            offtoon.append(0)
        # elif diff == np.nan:
            # offtoon.append(0)
    offtoon_array = np.array(offtoon)
    offtoon_boolean = offtoon_array.astype(bool)
    return offtoon_boolean



hc_ic = alerts_detected[13,:,1]

hc_ic_alarm = offon(hc_ic)


# date = df.drop([0,1])

# date = date.iloc[:,0]

# date.reset_index()

# date = date.reset_index(drop=True)

# hc_ic_pd = pd.Series(data=hc_ic)
# # date = pd.Series(date)

# hc_ic_pd_format = [date, hc_ic_pd]
# hc_ic_result = pd.concat(hc_ic_pd_format, axis=1)

# hc_ic_result_dti = hc_ic_result.set_index(0)

# hc_ic_result = hc_ic_result.rename(columns={"Unnamed: 0":"Datetime"})
# hc_ic_result = hc_ic_result.rename(columns={0:"value"})

# tss = pd.Series(hc_ic,index=hc_ic_result["Datetime"])
# hc_ic_dt = hc_ic_result.set_index('Datetime')

# hc_ic_dt_ps = pd.Series(hc_ic_dt)

# dts = hc_ic_result.iloc[:,1]
# date_index = hc_ic_result.iloc[:,0]

# trigger_comprehensive(tss)

# idx = pd.date_range('2018-01-01', periods=5, freq='H')
# ts = pd.Series(range(len(idx)), index=idx)


def deadzones(data,t,t_l, t_h,limit_type ="upper"):
    tv = []
    switch = 0
    for i in range(len(data)):
        if limit_type == "upper":
            if data[i] == np.nan:
                data[i] = data[i-1]
            if data[i]>=t and switch ==0:
                tv.append(1)   #threshold value
                switch = 1
            if data[i]<t and data[i]>t_l and switch==1: 
                tv.append(0)
                switch =1
            if data[i]<t and data[i]>t_l and switch ==0:
                tv.append(0)
                switch =0
            if data[i]<t_l :
                tv.append(0)
                switch =0
            if data[i] >=t and switch ==1:
                tv.append(0)
                switch =1
    return tv
       #write for tv 0
