
import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

#3 days
def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

df_csv = pd.read_csv("./data/pani_6month.csv")

df1 = df_csv.drop([0,1,2], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)
df1 = df1.fillna(method='ffill')

title  = df_csv.iloc[0,:]
title = np.array(title)
title = title[1:]

# df1 = df1.drop([0],axis=0)
df_array = df1.to_numpy()
df_array_hr = df_array[range(0,len(df_array),60),:]

# df_array_hr = df_array
df_array_hr = np.array(df_array_hr, dtype=np.float)
#Differencing to avoid seasonality
diff_array = np.diff(df_array_hr,axis=0)

cut_off_range = len(df_array_hr)
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)

# plot_pacf(df_array[:,14],lags=70)

#Parameters
forecast_days = 14
order = 50
title_list = df1.columns.tolist()

#Model fitting
def model_fitting(d_train, order,forecast_days = 14):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,min_order,max_order,forecast_days=14):
    mse_list = []
    for i in range(min_order,max_order+1):
        print(i)
        pred = model_fitting(d_train, i)
        # pred = pred[:,interest]
        d_test = d_test[:forecast_days,:]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)

    
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]


minimum_order = 20
maximum_order = 25
Quantity_interested = 33


mse_list = model_selection(data_train, data_test, minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1


mse_dictionary = {"mse":mse_list, "min_mse":min_mse_order}


import pickle
# pickle.dump( mse_dictionary, open( "save_mse_min_20__max_30_6months.p", "wb" ) )

mse_dict = {}
min_mse_list = []

#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    # mse_list = model_selection(data_train, data_test, Quantity_interested,minimum_order,maximum_order)
    # mse_dict["{}".format(kk)] = mse_list
    # min_mse_order = np.argmin(mse_list,axis=0)+1
    min_order_for_Quantity = min_mse_order[kk]
#    act_fit_ci(data_train,min_order_for_Quantity, Quantity_interested)
    # min_mse_list.append(min_order_for_Quantity)
    #Manually overide order
    # min_mse_order_interest = 40
    print("Order Choosen {} for {}".format(min_order_for_Quantity,title[kk]))
    
