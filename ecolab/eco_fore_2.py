#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from math import sqrt
from statsmodels.tsa.vector_ar.var_model import forecast_interval


def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

df = pd.read_excel("./data/Pani_Export_All_2_Weeks_0828to0911_2020.xls")

df1 = df.drop([0,1], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)
df1 = df1.fillna(method='ffill')


title  = df.iloc[0,:]
title = np.array(title)
title = title[1:]
df_array = df1.to_numpy()

#Differencing to avoid seasonality
diff_array = np.diff(df_array,axis=0)
cut_off_range = len(df1.values)

#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)

# plot_pacf(df_array[:,14],lags=70)
#Parameters
forecast_days = 14
order = 32


# model = VAR((data_train))
# model_fit = model.fit(order)
# ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)


# ye_for = ye[0]
# ye_for_l = ye[1]
# ye_for_u = ye[2]



# dict_mse = []

# list_dict_mse = []


# for i in range(0,df_array.shape[1]):
#     mse = mean_squared_error(ye_for[:,i], data_test[0:forecast_days,i])
#     dict_mse.append({i:mse}) 
#     list_dict_mse.append({order:dict_mse})


# forex = np.arange(1,forecast_days+1)

# for kk in range(0,df_array.shape[1]):
#     fig, ax = plt.subplots()
#     ax.plot(forex, ye_for[:,kk],color='k',label="pred")
#     ax.plot(forex,data_test[0:14,kk],color='b',label = "test")
#     ax.legend()



title_list = df1.columns.tolist()

#Model fitting
def model_fitting(d_train, order,forecast_days = 14):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,min_order,max_order,forecast_days=14):
    mse_list = []
    for i in range(min_order,max_order+1):
        print(i)
        pred = model_fitting(d_train, i)
        # pred = pred[:,interest]
        d_test = d_test[:forecast_days,:]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
    ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
    ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
    ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
    #Extracting confidence interval
    
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    ye_80_for, ye_80_for_l, ye_80_for_u = ye_80[0], ye_80[1], ye_80[2]
    ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
    ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2]
    
    fig, ax = plt.subplots()
    x = np.arange(1,15)
    ax.plot(x,ye_95_for[:,Quantity_interested], color='turquoise',label="pred")
    ax.plot(x,data_test[:forecast_days,Quantity_interested], color = 'k', label="true")
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
    ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=title[Quantity_interested],b=Quantity_interested))
    ax.legend()

minimum_order = 1
maximum_order = 30
Quantity_interested = 33

mse_list = model_selection(data_train, data_test, minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1

mse_dict = {}
min_mse_list = []

#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    # mse_list = model_selection(data_train, data_test, Quantity_interested,minimum_order,maximum_order)
    # mse_dict["{}".format(kk)] = mse_list
    # min_mse_order = np.argmin(mse_list,axis=0)+1
    min_order_for_Quantity = min_mse_order[kk]
    act_fit_ci(data_train,min_order_for_Quantity, Quantity_interested)
    # min_mse_list.append(min_order_for_Quantity)
    #Manually overide order
    # min_mse_order_interest = 40
    print("Order Choosen {} for {}".format(min_order_for_Quantity,title[kk]))
        
#Notes:
#order 12 for prop 27, 17, 26, 27
#order 12 not doing good for 8, 15


# act_fit_ci(data_train,min_mse_order_interest, Quantity_interested)
# mse_d = pickle.load(open("save_mse_min_1__max_30_2weeks","rb"))
# pickle.dump( save_mse_x, open( "save.p", "wb" ) )