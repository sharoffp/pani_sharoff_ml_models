
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_excel("Pani_Export_All_2_Weeks_0828to0911_2020.xls")


df1 = df.drop([0,1], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)


title  = df.iloc[0,:]
title = np.array(title)
title = title[1:]
df_array = df1.values


#=============Ploting=================

# for i in range(0, df_array.shape[1]):
#     fig, ax = plt.subplots()
#     ax.plot(df_array[:,i])
#     ax.set_title("{}".format(title[i]))
#     ax.set_xlabel("Time steps")
#     ax.set_ylabel("Value")

#=============Evaluating the no. of missing values===========
list_nan = []
for i in range(0,df_array.shape[1]):
    temp = df_array[:,i]
    list_nan.append(pd.isnull(temp).sum())


#Loading alerts ecolabs
alerts_df = pd.read_excel("ecolab_alert_list.xlsx")


#Separating different thresholds
Tags = alerts_df["Tag"].values
low_critical = alerts_df["Low Critical"]



match = []
# match_index = {}
match_in =[]
# for i in range(len(Tags)):
sub = "Outlet pressure"
    # s = title
for i in range(len(title)):
    str_temp = title[i]
    if sub.lower() in str_temp.lower():
        print(1)
        match.append(str_temp)
        match_in.append(i)

match_title = title[match_in]

    

        # print ([s for s in title if sub in s])
match_index= {"inlet pressure": match_in}


softener_high_critical = alerts_df.iloc[6,:]["High Critical"]
softener_low_critical = alerts_df.iloc[6,:]["Low Critical"]
softener_high_informational = alerts_df.iloc[6,:]["High Informational"]


softener_inlet_pressure = df_array[:,172]
softener_inlet_pressure  = softener_inlet_pressure.reshape((-1,1))
alerts_high_information = softener_inlet_pressure>softener_high_informational
alerts_low_critical = softener_inlet_pressure < softener_low_critical
alerts_high_critical = softener_inlet_pressure > softener_high_critical
# df_six_months = pd.read_excel("Pani_6Month_Complete_01302020_to_09112020.xlsx")
total_h_info_alerts  = alerts_high_information.sum()
total_l_crit_alerts = alerts_low_critical.sum() 
total_h_crit_alerts = alerts_high_critical.sum()


plt.plot(softener_inlet_pressure)
plt.axhline(y=softener_high_critical, color='r')
plt.axhline(y=softener_low_critical, color='g')
plt.axhline(y=softener_high_informational, color='k')
