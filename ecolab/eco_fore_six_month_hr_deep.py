#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval



import tensorflow as tf
import numpy as np
import datetime
from numpy import array
from numpy import split
import matplotlib.pyplot as plt
import time

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
from keras.layers import Dropout
from keras.layers import Conv1D
from keras.layers import MaxPooling1D
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler


# from data_func import data

#3 days
def split_data(data, ratio=0.9867256637168141):
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

df_csv = pd.read_csv("./data/pani_6month.csv")

df1 = df_csv.drop([0,1,2], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)
df1 = df1.fillna(method='ffill')


title  = df_csv.iloc[0,:]
title = np.array(title)
title = title[1:]

# df1 = df1.drop([0],axis=0)
df_array = df1.to_numpy()
df_array_hr = df_array[range(0,len(df_array),60),:]

# df_array_hr = df_array
df_array_hr = np.array(df_array_hr, dtype=np.float)

diff_array = np.diff(df_array_hr,axis=0)


cut_off_range = len(df_array_hr)
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)

# plot_pacf(df_array[:,14],lags=70)

#Parameters
forecast_days = 24
order = 72

title_list = list(title)
title_list = [int(strings) for strings in title_list]

required_list = [3213,3218,3219,3220,3113,3118,3119,3120,101213,101218,101219,101220,101113,101118,101119,101120,100937,100938,100939]
index_req_list = [title_list.index(item) for item in required_list]

# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)

org_train = data_train
org_test = data_test

n_input = 48
parameter = index_req_list[0]
n_output = forecast_days

#org
org_train_arange = array(split(org_train,1))
org_val_arange = array(split(org_test,1))
org_x, org_y = to_supervised(org_train_arange,n_input,n_out=n_output,req_parameter=parameter)
org_val_x, org_val_y = to_supervised(org_val_arange,n_input,n_out=n_output,req_parameter=parameter)

#Naming
org_train_x = org_x

# verbose, epochs, batch_size = 1, 20, 10
# n_timesteps, n_features, n_outputs = x.shape[1], x.shape[2], y.shape[1]

org_n_timesteps, org_n_features, org_n_outputs = org_train_x.shape[1], org_train_x.shape[2], org_y.shape[1]
print("n_output:", org_n_outputs)
# reshape output into [samples, timesteps, features]
# train_y = y.reshape((y.shape[0], y.shape[1], 1))
# val_y = val_y.reshape((val_y.shape[0], val_y.shape[1],1))

org_train_y = org_y.reshape((org_y.shape[0], org_y.shape[1], 1))
org_val_y = org_val_y.reshape((org_val_y.shape[0], org_val_y.shape[1],1))


start_time = time.time()

from keras.layers import Flatten
model = Sequential()
model.add(LSTM(12, activation='relu', input_shape=(org_n_timesteps,org_n_features)))
model.add(RepeatVector(org_n_outputs))
# model.add(LSTM(10, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(9, activation='relu')))
model.add(TimeDistributed(Dense(5, activation='relu')))
model.add(TimeDistributed(Dense(1)))


epochs = 2000
batch_size = len(org_train_x)
print("batch_size",batch_size)
verbose = 1

model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(org_train_y),np.shape(org_train_x))
print("Shape of val_y & val_x:",np.shape(org_val_y),np.shape(org_val_x))

model.fit(org_train_x, org_train_y,validation_data=(org_val_x, org_val_y), epochs=epochs, batch_size=batch_size, verbose=verbose)

training_time = start_time - time.time()
print("Total time taken", training_time)


i=1
x_test = org_val_x[i,:,:]
x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))
yhat_arange = model.predict(x_test_reshape, verbose=0)

y_pred = yhat_arange.reshape((-1,1))
y_test = org_val_y.reshape((-1,1))

plt.plot(y_pred)
plt.plot(y_test)
plt.show()

def inv_data(initial_value,diff_value, original_stream):
    initial_value = np.reshape(original_stream[0,:],(1,-1))
    concat_diff = np.concatenate((initial_value,diff_value), axis=0)
    inv_data = np.cumsum(concat_diff, axis =0)
    return inv_data

initial_value = np.reshape(df_array_hr[0,:],(1,-1))
conc_data_train = np.concatenate((initial_value,data_train), axis=0)
inv_data_train = np.cumsum(conc_data_train, axis = 0)

init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
conc_data_forecast = np.concatenate((init_forecast,ye_95_for), axis=0)
conc_data_fore_ciu = np.concatenate((init_forecast,ye_95_for_u), axis=0)
conc_data_fore_cil = np.concatenate((init_forecast,ye_95_for_l), axis=0)
conc_data_test = np.concatenate((init_forecast,data_test), axis=0)

inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
inv_test_data = np.cumsum(conc_data_test, axis = 0)
