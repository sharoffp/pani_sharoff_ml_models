#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np
from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

df = pd.read_csv('ut_export_alltime_copy.csv')
df = df.drop(df.index[0:2])

#Columns with a lot of NaNs
cols = [7,13,26,27]
df.drop(df.columns[cols],axis=1,inplace=True)

print("dropping c_b,p_cfi, dp_cf, S_sum")

df_req = df
#Data Time columns
cols = [0,1]
df_req.drop(df_req.columns[cols],axis=1,inplace=True)

# df_c1 = df_req.iloc[:,8]

for col in df_req:
    df_req[col] = pd.to_numeric(df_req[col], errors='coerce')
df_req = df_req.interpolate(method='linear', limit_direction='forward', axis=0)

#Converting to array
df_array = df_req.to_numpy()


#Differencing to avoid seasonality
diff_array = np.diff(df_array,axis=0)
cut_off_range = 5000
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)

# plot_pacf(df_array[:,14],lags=70)

#Parameters
forecast_days = 14
order = 12
#order 12 for prop 27, 17, 26, 27
#order 12 not doing good for 8, 15

model = VAR((data_train))
model_fit = model.fit(order)

ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)

ye_for = ye[0]
ye_for_l = ye[1]
ye_for_u = ye[2]

# plt.plot(ye_for[:,27])
# plt.plot(data_test[0:14,27])

prop = 26
plot_pacf(data_train[:,prop])

# x = np.arange(1,forecast_days+1)
# fig, ax = plt.subplots()
# ax.plot(x,ye_for[:,prop])
# ax.fill_between(x, ye_for_l[:,prop], ye_for_u[:,prop], color='b', alpha=0.1)
# ax.plot(x,data_test[0:forecast_days,prop], color='k')
# ax.legend(["order: {}; prop:{}".format(order,prop)])

dict_mse = []
list_dict_mse = []

for i in range(0,28):
    mse = mean_squared_error(ye_for[:,i], data_test[0:forecast_days,i])
    dict_mse.append({i:mse}) 
    list_dict_mse.append({order:dict_mse})

#Differenced plots
title_list = [
        "Flow Transmitter  [FT - 103] [m³/h]	",
        "FT for RO to PWST [FT - 104] [m³/h]	",
        "FT at ERT to RWST [FT - 105] [m³/h]	",
        "Temp Of CF Inlet [TT - 001] [°C]",	
        "Cond. at CF inlet [AIT -  003] [µS/cm]",	
        # "Cond. At ERT to RWST [AIT - 005] [µS/cm]",	
        "Cond. At Product water  [AIT - 006] [µS/cm]",   
        "PH of CF inlet [AIT - 001] [-]",    
        "ORP at CF inlet [AIT - 002] [mV]",  
        "PH of ERT To RWST [AIT - 013] [-]", 
        "PH of Product water [AIT - 007] [-]	",
        # "CF Inlet PT [kg/cm²]",
        "RO DPIT [DPIT - 101] [kg/cm²]", 
        "ERT outlet PT [PT - 005] [kg/cm²]", 
        "HPP outlet Pt [PT - 004] [kg/cm²]",  	
        "CF outlet PT [PT - 001] [kg/cm²]",	
        "HPP Load [A]",	 
        "HPP Power [kW]", 
        "Reject water tank level [LT - 005] [m]", 
        "Product water tank level [LT-  007] [m]",	
        "Totalizer RO A, B & C [m3]",
        "Totalizer Product outlet [FT - 108] [m³]",  
        "Totalizer Reject outlet [FT - 109] [m³]",	
        "Totalizer CF Inlet [FT - 111] [m³]",
        # "Differential Pressure [kg/cm²]",
        # "Conservation of Salt Flow [kg/h]",  
        "Conservation of Water Flow [m³/h]",    
        "A Average Flux [L/m²/h]",	
        "RO-A HPP Pressure Boost [kg/cm²]",
        "RO-A Membrane Recovery [%]",
        "A Salt Passage [%]",
        "A Salt Rejection [%]"
        ]


title_list_without_units = [
        "Flow Transmitter ",
        "FT for RO to PWST ",
        "FT at ERT to RWST ",
        "Temp Of CF Inlet ",	
        "Cond. at CF inlet ",	
        # "Cond. At ERT to RWST [AIT - 005] [µS/cm]",	
        "Cond. At Product water ",   
        "PH of CF inlet ",    
        "ORP at CF inlet ",  
        "PH of ERT To RWST", 
        "PH of Product water ",
        # "CF Inlet PT [kg/cm²]",
        "RO DPIT ", 
        "ERT outlet PT ", 
        "HPP outlet Pt ",  	
        "CF outlet PT ",	
        "HPP Load",	 
        "HPP Power", 
        "Reject water tank level", 
        "Product water tank level ",	
        "Totalizer RO A, B & C ",
        "Totalizer Product outlet ",  
        "Totalizer Reject outlet ",	
        "Totalizer CF Inlet ",
        # "Differential Pressure [kg/cm²]",
        # "Conservation of Salt Flow [kg/h]",  
        "Conservation of Water Flow ",    
        "A Average Flux ",	
        "RO-A HPP Pressure Boost ",
        "RO-A Membrane Recovery ",
        "A Salt Passage ",
        "A Salt Rejection "
        ]

for i in range(0,len(data_train[0,:])):
    len_data_train = len(data_train)
    x = np.arange(1,len_data_train+1)
    fore_x = np.arange(len_data_train+1,len_data_train+forecast_days+1)
    fig, ax = plt.subplots()
    ax.plot(fore_x,ye_for[:,i])
    ax.fill_between(fore_x, ye_for_l[:,i], ye_for_u[:,i], color='b', alpha=0.1)
    ax.plot(fore_x,data_test[0:forecast_days,i], color='k')
    ax.plot(x[4810:],data_train[4810:,i], color='k')
    ax.set_title("{}".format(title_list[i]))
    ax.set_xlabel("Time steps (in hrs)")
    ax.set_ylabel("Value")
    ax.legend([" Forecasted value", "True value"])
#     plt.savefig("forecast_for_property {}.pdf".format(title_list_without_units[i]))

# plt.plot(data_test[0:forecast_days,1])

#Inverting the difference
initial_value = np.reshape(df_array[0,:],(1,-1))
conc_data_train = np.concatenate((initial_value,data_train), axis=0)
inv_data_train = np.cumsum(conc_data_train, axis = 0)

init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
conc_data_forecast = np.concatenate((init_forecast,ye_for), axis=0)
conc_data_fore_ciu = np.concatenate((init_forecast,ye_for_u), axis=0)
conc_data_fore_cil = np.concatenate((init_forecast,ye_for_l), axis=0)
conc_data_test = np.concatenate((init_forecast,data_test), axis=0)

inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
inv_test_data = np.cumsum(conc_data_test, axis = 0)

len_data_train = len(inv_data_train)
x = np.arange(1,len_data_train+1)
fore_x = np.arange(len_data_train+1,len_data_train+forecast_days+1)
fig, ax = plt.subplots()
ax.plot(fore_x,inv_forecast[1:,prop])
ax.fill_between(fore_x, inv_forecast_l[1:,prop], inv_forecast_u[1:,prop], color='b', alpha=0.1)
ax.plot(fore_x,inv_test_data[1:forecast_days+1,prop], color='k')
ax.plot(x[4810:],inv_data_train[4810:,prop], color='k')
# ax.xticks("Time steps")
# ax.yticks("Value")
ax.legend([" Forecasted value", "True value"])
# plt.savefig("demo_image.pdf")


# plt.plot(df_array[4831:4845, prop])
# plt.plot(inv_test_data[1:15,prop])


fig, ax = plt.subplots()
ax.plot(fore_x, inv_forecast[1:,prop])
ax.plot(fore_x, inv_test_data[1:forecast_days+1,prop])
ax.fill_between(fore_x, inv_forecast_u[1:, prop], inv_forecast_l[1:, prop], alpha = 0.2)
ax.plot(x[4710:],inv_data_train[4710:,prop], color='k')


#sloppy_versions_to_get_the_logics
def offon(data):
    data_int = np.array(data)
    data_int = data_int.astype(int)
    offtoon = []
    offtoon.append(0) #Initial state
    if data_int[0] == np.nan:
        data_int[0] = 0
    for i in range(1,len(data_int)):
        if data_int[i] == np.nan:
            data_int[i] = data_int[i-1]
        diff = (data_int[i]-data_int[i-1])
        if diff == 1:
            offtoon.append(1)
        elif diff ==0:
            offtoon.append(0)
        else:
            offtoon.append(0)
        # elif diff == np.nan:
            # offtoon.append(0)
    offtoon_array = np.array(offtoon)
    offtoon_boolean = offtoon_array.astype(bool)
    return offtoon_boolean

#sloppy_version_to_get_the_logics
def deadzones(data,t,t_l, t_h,limit_type ="upper"):
    tv = []
    switch = 0
    for i in range(len(data)):
        if limit_type == "upper":
            if data[i] == np.nan:
                data[i] = data[i-1]
            if data[i]>=t and switch ==0:
                tv.append(1)   #threshold value
                switch = 1
            if data[i]<t and data[i]>t_l and switch==1: 
                tv.append(0)
                switch =1
            if data[i]<t and data[i]>t_l and switch ==0:
                tv.append(0)
                switch =0
            if data[i]<t_l :
                tv.append(0)
                switch =0
            if data[i] >=t and switch ==1:
                tv.append(0)
                switch =1
    return tv


def solution_library():
    return "solution"


def limits():
    df_alerts = pd.read_excel("Ecolab_Alert_List_rev2.xlsx")
    

req_prop = 5 #conductivity at product

critical_ht_product_conductivity = 200
solution = "solution"

prod_cond = inv_forecast[1:,req_prop]
t_prod_cond = prod_cond>critical_ht_product_conductivity
off_on_alarm = offon(t_prod_cond)
off_on_alarm[2] = True
future_alarm_time = fore_x[off_on_alarm] - fore_x[0]

print("{} is forecasted to exceed the limit of {} in {} hours".format(title_list_without_units[req_prop],critical_ht_product_conductivity,future_alarm_time[0]))

Quantity_interested = 14
maximum_order = 40
minimum_order = 1

test_forecast = data_test[:forecast_days,:]

#MSE which supports broadcasting
def m_s_e(true,pred):
    n = np.shape(true)[0]
    diff = true -pred 
    sq_diff = np.square(diff)
    return (np.sum(sq_diff,axis=0))/n

#Model fitting
def model_fitting(d_train, order):
    model = VAR((d_train))    
    model_fit = model.fit(order)    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    return ye[0]


#Model selection
def model_selection(d_train,d_test,interest,min_order,max_order):
    mse_list = []
    for i in range(min_order,max_order+1):
        pred = model_fitting(d_train, i)
        # inter_pred = pred[:,interest]
        mse = m_s_e(d_test,pred)
        mse_list.append(mse)
        print("order {} completed".format(i))
    # min_mse_order = np.argmax(mse_list)+1
    return mse_list 

#Actual fitting
def act_fit_ci(data_train,min_mse_order_interest,Quantity_interested,forecast_days=14):
    model = VAR((data_train))    
    model_fit = model.fit(min_mse_order_interest)    
    ye_95 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.05)
    ye_80 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.2)
    ye_70 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.3)
    ye_60 = model_fit.forecast_interval(model_fit.y, steps = forecast_days,alpha=0.4)
    #Extracting confidence interval
    
    ye_95_for, ye_95_for_l, ye_95_for_u = ye_95[0], ye_95[1], ye_95[2]
    ye_80_for, ye_80_for_l, ye_80_for_u = ye_80[0], ye_80[1], ye_80[2]
    ye_70_for, ye_70_for_l, ye_70_for_u = ye_70[0], ye_70[1], ye_70[2]
    ye_60_for, ye_60_for_l, ye_60_for_u = ye_60[0], ye_60[1], ye_60[2]
    
    fig, ax = plt.subplots()
    x = np.arange(1,15)
    ax.plot(x,ye_95_for[:,Quantity_interested])
    ax.plot(x,test_forecast[:,Quantity_interested])
    ax.fill_between(x,ye_95_for_l[:,Quantity_interested], ye_95_for_u[:,Quantity_interested], color='b', alpha=.1)
    ax.fill_between(x, ye_80_for_l[:,Quantity_interested], ye_80_for_u[:,Quantity_interested], color='y', alpha=.1)
    ax.fill_between(x, ye_70_for_l[:,Quantity_interested], ye_70_for_u[:,Quantity_interested], color='g', alpha=.1)
    ax.fill_between(x, ye_60_for_l[:,Quantity_interested], ye_60_for_u[:,Quantity_interested], color='r', alpha=.1)
    ax.set_title("Label: {a} Index{b}".format(a=columns_names[Quantity_interested],b=Quantity_interested))



mse_list = model_selection(data_train,test_forecast, Quantity_interested,minimum_order,maximum_order)
min_mse_order = np.argmin(mse_list,axis=0)+1




#Parameters
for kk in range(data_train.shape[1]):    
    Quantity_interested = kk
    min_mse_order_interest = min_mse_order[Quantity_interested]
    #Manually overide order
    # min_mse_order_interest = 40
    print("Order Choosen {}".format(min_mse_order_interest))
    act_fit_ci(data_train,min_mse_order_interest, Quantity_interested)


###############Checking##############

# calculate the spearmans's correlation between two variables
from numpy.random import randn

from scipy.stats import spearmanr

corr_spearman, _ = spearmanr(data_train,axis = 0)



#Calculating Pearsons correlation

from scipy.stats import pearsonr
# prepare data

# calculate Pearson's correlation

corr_spearman = np.corrcoef(np.transpose(data_train))



# uncorrelated_info = [0,4,6,8,11,13,15,17,20,22,24,32,33,36,34,38,42,44,50,52,53,54,60,66,67]
# df_clean_with_impluse  = df_no_switch_sp.drop(df_no_switch_sp.columns[uncorrelated_info],axis=1)

# for i in range(3):
#     fig, ax = plt.subplots()
#     ax.plot(df_clean.iloc[:,i])


# cl = df_clean.columns
# print(len(cl))

# plot_pacf(data_train[:,1])
# plt.plot(data_train[:,1])

# pacf
# plt.plot(ye_for_l[:,Quantity_interested])
# plt.plot(ye_for_u[:,Quantity_interested])
# plt.plot(test_forecast[:,Quantity_interested])
# plt.title(columns_names[Quantity_interested])

# for j in range(ye_for.shape[1]):
#     plt.plot(ye_for[:,j])
#     plt.plot(test_forecast[:,j])
#     plt.title(columns_names[j])
#     plt.show()

title_list_without_units = [
        "Flow Transmitter ", - not present
        "FT for RO to PWST ", - not present
        "FT at ERT to RWST ", - 
        "Temp Of CF Inlet ",	
        "Cond. at CF inlet ",	
        # "Cond. At ERT to RWST [AIT - 005] [µS/cm]",	
        "Cond. At Product water ",   
        "PH of CF inlet ",    
        "ORP at CF inlet ",  
        "PH of ERT To RWST", 
        "PH of Product water ",
        # "CF Inlet PT [kg/cm²]",
        "RO DPIT ", 
        "ERT outlet PT ", 
        "HPP outlet Pt ",  	
        "CF outlet PT ",	
        "HPP Load",	 
        "HPP Power", 
        "Reject water tank level", 
        "Product water tank level ",	
        "Totalizer RO A, B & C ",
        "Totalizer Product outlet ",  
        "Totalizer Reject outlet ",	
        "Totalizer CF Inlet ",
        # "Differential Pressure [kg/cm²]",
        # "Conservation of Salt Flow [kg/h]",  
        "Conservation of Water Flow ",    
        "A Average Flux ",	
        "RO-A HPP Pressure Boost ",
        "RO-A Membrane Recovery ",
        "A Salt Passage ",
        "A Salt Rejection "
        ]











