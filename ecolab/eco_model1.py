#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import datetime
from numpy import array
from numpy import split
import pandas as pd

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed

# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:1440], data[1440:2880]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/1440))
# 	test = array(split(test, len(test)/1440))
	test = array(split(test, 1))
	return train, test

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=1,req_parameter=0):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, req_parameter])
		# move along one time step
		in_start += 1
	return array(X), array(y)

df_csv = pd.read_csv("./data/pani_6month.csv")
df1 = df_csv.drop([0,1,2], axis= 0)
df1 = df1.drop(df1.columns[[0]],axis = 1)
df1 = df1.fillna(method='ffill')

title  = df_csv.iloc[0,:]
title = np.array(title)
title = title[1:]

# df1 = df1.drop([0],axis=0)
df_array = df1.to_numpy()
df_array_hr = df_array[range(0,len(df_array),60),:]

# df_array_hr = df_array
df_array_hr = np.array(df_array_hr, dtype=np.float)

#Differencing to avoid seasonality
diff_array = np.diff(df_array_hr,axis=0)

# diff_array = df_array_hr
cut_off_range = len(df_array_hr)
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
# data_train, data_test = split_data(df_ar_no_out)


data_reshaped = df_ar_no_out 

# data_arange = np.arange(1000)
# data_reshaped = data_arange.reshape(100,-1)

n_input = 10
req_parameter = 5
n_output = 1

train_arange = array(split(data_reshaped,1))

x,y = to_supervised(train_arange,n_input)


#Naming
train_x = x
# verbose, epochs, batch_size = 1, 20, 10
n_timesteps, n_features, n_outputs = x.shape[1], x.shape[2], y.shape[1]
print("n_output:", n_outputs)
# reshape output into [samples, timesteps, features]
train_y = y.reshape((y.shape[0], y.shape[1], 1))

# sp_coeff = train_arange[0,:,:]
# from scipy import stats
# coer_mat, p_value = stats.spearmanr(sp_coeff)

# define model
model = Sequential()
model.add(LSTM(200, activation='relu', input_shape=(n_timesteps, n_features)))
model.add(RepeatVector(n_outputs))
model.add(LSTM(200, activation='relu', return_sequences=True))
model.add(TimeDistributed(Dense(100, activation='relu')))
# model.add(TimeDistributed(Dense(5, activation='relu')))
model.add(TimeDistributed(Dense(1)))

epochs = 100
batch_size = len(x)
print("batch_size",batch_size)
verbose = 1

log_dir = "logs/trial/arange_fore_withval/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.compile(loss='mse', optimizer='adam')
# fit network
print("Shape of train_y & train_x:",np.shape(train_y),np.shape(train_x))
model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, callbacks=[tensorboard_callback])

j = 4

x_test = x[j,:,:]
x_test_reshape = x_test.reshape((1,x_test.shape[0],x_test.shape[1]))

yhat_arange = model.predict(x_test_reshape, verbose=0)
ytest_arange = y[j]

print("pred: ", yhat_arange)
print("true:", ytest_arange)
