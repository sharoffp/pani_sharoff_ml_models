This package has codes and data (training and test data) to be used in the web app for forecasting different properties. 

The following is the high level overview of the structure

flask_app : Folder containing all web app related code
web_app.py : Main file for the flask web app. 

To run the app execute

```
python3 web_app.py
```

templates: folder containing html templates

static: folder containing javascript and css artifacts

forecast.py : contains methods that the api relies on