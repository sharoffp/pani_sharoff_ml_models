function plot_ly_ts(json_received){



    // diff_fore = ye_for[:,prop]
    // diff_prev_x = x[4810:]
    // diff_prev_y = data_train[4810:,prop]
    // diff_test = data_test[0:forecast_days,prop]
    // diff_fore_cil = ye_for_l[:,prop]
    // diff_fore_ciu = ye_for_u[:,prop]
console.log(json_received)
 
var diff_forey = json_received['diff_fore']
var diff_prev_x = json_received['diff_prev_x']
var diff_prev_y = json_received['diff_prev_y']
var diff_test = json_received['diff_test']
var diff_fore_cil = json_received['diff_fore_cil']
var diff_fore_ciu = json_received['diff_fore_ciu']

// console.log(json_received)

var forex = json_received['fore_x'];
var forey = json_received['fore_y'];

var y_test =json_received['fore_y_test'];

var fore_y_cil = json_received['fore_y_cil'];
var fore_y_ciu = json_received['fore_y_ciu'];

var fore_y_test = json_received['fore_y_test'];

var prev_x_data = json_received['prev_x_data'];
var prev_y_data = json_received['prev_y_data'];

var ci_l = fore_y_cil;
var ci_u = fore_y_ciu;

var x_true = forex;

var title_plot = json_received['title'];
console.log(title_plot);

var x_label = "time steps";
var y_label = "value";
// Parsing json

// var x_true = json_received['trace4'];
// var y_true = json_received['trace4'];

// var x_pred = json_received['trace4'];
// var ci_l = json_received['a_ci_l'];
// var ci_u = json_received['a_ci_u'];



var ci_l_rev = ci_l.slice().reverse(); 

var y_ci = ci_u.concat(ci_l_rev);
var x_true_rev = x_true.slice().reverse();
var x_ci = x_true.concat(x_true_rev);


var diff_ci_l_rev = diff_fore_cil.slice().reverse();

var diff_ci_u = diff_fore_ciu.slice()
var diff_ci = diff_ci_u.concat(diff_ci_l_rev)

// console.log(y_ci)
console.log(diff_ci_l_rev)


// console.log(y_ci)
// console.log(x_ci)
// // var trace3 = {
// //   x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 10, 9, 8, 7, 6,5,4,3,2,1], 
// //   y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, -1, 1, 3, 1, -0.5,2,4,6,8,10], 
// //   // x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 1,2,3,4,5,6,7,8,9,10], 
// //   // y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, 10, 8, 6, 4, 2,-0.5,1,3,1,-1], 
// //   fill: "tozerox", 
// //   fillcolor: "rgba(231,107,243,0.2)", 
// //   line: {color: "transparent"}, 
// //   // traceorder: 'reversed',
// //   name: "Fair", 
// //   showlegend: false, 
// //   type: "scatter"
// // };


var trace3 = {
  x: x_ci, 
  y: y_ci, 
  // x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 1,2,3,4,5,6,7,8,9,10], 
  // y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, 10, 8, 6, 4, 2,-0.5,1,3,1,-1], 
  fill: "tozeroy", 
  fillcolor: "rgba(231,107,243,0.2)", 
  line: {color: "transparent"}, 
  // traceorder: 'reversed',
  name: "Confidence band", 
  showlegend: false, 
  type: "scatter"
};



var trace4 = {
  x: x_ci, 
  y: diff_ci, 
  // x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 1,2,3,4,5,6,7,8,9,10], 
  // y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, 10, 8, 6, 4, 2,-0.5,1,3,1,-1], 
  fill: "tozeroy", 
  fillcolor: "rgba(231,107,243,0.2)", 
  line: {color: "transparent"}, 
  // traceorder: 'reversed',
  name: "Confidence band", 
  showlegend: false, 
  type: "scatter"
};







console.log(forex.slice(5,10))

var trace6 = {
  x: forex,  
  y: forey, 
  line: {color: "rgb(231,107,243)"}, 
  mode: "lines", 
  name: "Predicted", 
  type: "scatter"
};


var trace7 = {
  x: forex,  
  y: y_test, 
  line: {color: "rgb(0,0,0)"}, 
  mode: "lines", 
  name: "Actual", 
  type: "scatter"
};


var trace8 = {
  x: prev_x_data,  
  y: prev_y_data, 
  line: {color: "rgb(38, 78, 248)"},
  name: "Actual", 
  mode: "lines",  
  type: "scatter"
};



var trace9 = {
  x: forex,  
  y: diff_forey, 
  line: {color: "rgb(231,107,243)"}, 
  mode: "lines", 
  name: "Predicted", 
  type: "scatter"
};


var trace10 = {
  x: forex,  
  y: diff_test, 
  line: {color: "rgb(0,0,0)"}, 
  mode: "lines", 
  name: "Actual", 
  type: "scatter"
};


var trace11 = {
  x: diff_prev_x,  
  y: diff_prev_y, 
  line: {color: "rgb(38, 78, 248)"}, 
  mode: "lines", 
  name: "Ideal", 
  type: "scatter"
};



// var trace7 = {
//   x: [6, 7, 8, 9, 10], 
//   y: x_true.slice(5), 
//   line: {color: "rgb(249,207,143)"}, 
//   mode: "lines", 
//   name: "fun", 
//   type: "scatter"
// };




// var data = [trace6];


var data = [trace3, trace6, trace7, trace8];
var data1 = [trace4, trace9, trace10];

var layout = {
  paper_bgcolor: "rgb(255,255,255)", 
  plot_bgcolor: "rgb(255,255,255)", 
  title: {
    text:title_plot,
    font: {
      family: 'Courier New, monospace',
      size: 24
    },
    xref: 'paper',
    x: 0.05,
  },
  xaxis: {
    gridcolor: "rgb(255,255,255)", 
    // range: [1, 10], 
    title: {
      text: 'time steps',
      font: {
        family: 'Courier New, monospace',
        size: 18,
        color: '#7f7f7f'
      }
    },
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }, 
  yaxis: {
    gridcolor: "rgb(255,255,255)", 
    title: {
      text: 'values',
      font: {
        family: 'Courier New, monospace',
        size: 18,
        color: '#7f7f7f'
      }
    },
    // text: "values"
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }
};


var layout1 = {
  paper_bgcolor: "rgb(255,255,255)", 
  plot_bgcolor: "rgb(255,255,255)", 
  title: {
    text:title_plot,
    font: {
      family: 'Courier New, monospace',
      size: 24
    },
    xref: 'paper',
    x: 0.05,
  },
  xaxis: {
    gridcolor: "rgb(255,255,255)", 
    // range: [1, 10], 
    title: {
      text: 'time steps',
      font: {
        family: 'Courier New, monospace',
        size: 18,
        color: '#7f7f7f'
      }
    },
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }, 
  yaxis: {
    gridcolor: "rgb(255,255,255)", 
    title: {
      text: 'Change in Values',
      font: {
        family: 'Courier New, monospace',
        size: 18,
        color: '#7f7f7f'
      }
    },
    // text: "values"
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }
};

Plotly.newPlot('chart', data, layout);
Plotly.newPlot('plot', data1, layout1);
}