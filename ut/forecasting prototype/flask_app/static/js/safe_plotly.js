function plot_ly_ts(json_received){


console.log(json_received)


// Parsing json

var x_true = json_received['trace4'];
var y_true = json_received['trace4'];

var x_pred = json_received['trace4'];
var ci_l = json_received['a_ci_l'];
var ci_u = json_received['a_ci_u'];

var ci_l_rev = ci_l.slice().reverse(); 

var y_ci = ci_u.concat(ci_l_rev);
var x_true_rev = x_true.slice().reverse();
var x_ci = x_true.concat(x_true_rev);
console.log(y_ci)
console.log(x_ci)
// var trace3 = {
//   x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 10, 9, 8, 7, 6,5,4,3,2,1], 
//   y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, -1, 1, 3, 1, -0.5,2,4,6,8,10], 
//   // x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 1,2,3,4,5,6,7,8,9,10], 
//   // y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, 10, 8, 6, 4, 2,-0.5,1,3,1,-1], 
//   fill: "tozerox", 
//   fillcolor: "rgba(231,107,243,0.2)", 
//   line: {color: "transparent"}, 
//   // traceorder: 'reversed',
//   name: "Fair", 
//   showlegend: false, 
//   type: "scatter"
// };


var trace3 = {
  x: x_ci, 
  y: y_ci, 
  // x: [ 1, 2, 3, 4, 5,6, 7, 8, 9, 10, 1,2,3,4,5,6,7,8,9,10], 
  // y: [ 10, 8, 6, 4, 2,1, 3, 5, 3, 1, 10, 8, 6, 4, 2,-0.5,1,3,1,-1], 
  fill: "tozerox", 
  fillcolor: "rgba(231,107,243,0.2)", 
  line: {color: "transparent"}, 
  // traceorder: 'reversed',
  name: "Fair", 
  showlegend: false, 
  type: "scatter"
};




var trace6 = {
  x: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 
  y: json_received['trace4'], 
  line: {color: "rgb(231,107,243)"}, 
  mode: "lines", 
  name: "Ideal", 
  type: "scatter"
};


var trace7 = {
  x: [6, 7, 8, 9, 10], 
  y: x_true.slice(5), 
  line: {color: "rgb(249,207,143)"}, 
  mode: "lines", 
  name: "fun", 
  type: "scatter"
};




// var data = [trace6]


var data = [trace3, trace6,trace7];
var layout = {
  paper_bgcolor: "rgb(255,255,255)", 
  plot_bgcolor: "rgb(255,255,255)", 
  xaxis: {
    gridcolor: "rgb(255,255,255)", 
    range: [1, 10], 
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }, 
  yaxis: {
    gridcolor: "rgb(255,255,255)", 
    showgrid: true, 
    showline: false, 
    showticklabels: true, 
    tickcolor: "rgb(127,127,127)", 
    ticks: "outside", 
    zeroline: false
  }
};
Plotly.newPlot('chart', data, layout);

}