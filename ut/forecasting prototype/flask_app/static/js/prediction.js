



$( document ).ready(function() {
let dropdown = $('#locality-dropdown');

dropdown.empty();

dropdown.append('<option selected="true" disabled>Choose the Property</option>');
dropdown.prop('selectedIndex', 0);

const url = '/api/dropdown';

// Populate dropdown with list of provinces
$.getJSON(url, function (data) {
  $.each(data, function (key, entry) {
    dropdown.append($('<option></option>').attr('value', entry.abbreviation).text(entry.name));
  })
});
});









$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
    if ($(document).scrollTop() > 50) {
    $('.nav a').addClass('shrink');
  } else {
    $('.nav a').removeClass('shrink');
  }
    if ($(document).scrollTop() > 50) {
    $('.navbar-brand').addClass('shrink');
  } else {
    $('.navbar-brand').removeClass('shrink');
  }
});


$('#predict_button').click(function(){
  var os = $("#locality-dropdown option:selected").val();
  console.log(os);
  $.get( '/api/predictor/'+os, function(response) {
      populate_concentration(response);
      // var os = $("#locality-dropdown option:selected").val();
      console.log(os);
  })
  .fail(function() {
    alert( "error" );
  });
});

// $('#predict_button').click(function(){
//   $.get( '/api/predictor', function(response) {
//       plot_ly_ts(response);
//   })
//   .fail(function() {
//     alert( "error" );
//   });
// });


// $('#predict_button').click(function(){

//   prediction_type = $('input[name="prediction_type_radio"]:checked').val();

//   $.get( '/api/predictor?prediction_type='+ prediction_type, function(response) {
//     if(prediction_type == "concentration"){
//       populate_concentration(response);
//     }
//     if(prediction_type == "cost"){
//       populate_cost(response);
//     }
//     if(prediction_type == "turbidity"){
//       populate_turbidity(response);
//     }
//   })
//   .fail(function() {
//     alert( "error" );
//   });
// });








function populate_concentration(response){

   $("#my_dataviz").empty('');
   $("#populated_result").empty('');
  //  table_rows = '';
  //  for (i = 0; i < response.predicted_fecl3.length; i++) {
  //   table_rows = table_rows + '<tr><th scope="row">' + (i+1).toString() + '</th><td>' + response.predicted_fecl3[i] + '</td><td>' + response.predicted_polymer[i] + '</td><td>' + response.predicted_flow_rate[i] + '</td></tr>';
  //  }

  // table_string = '<table class="table table-striped"><thead><tr><th scope="col">Predicted Days' 
  //              + '</th><th scope="col">Forecasted FeCl3 concentration (mg/L) </th><th scope="col">Forecasted Polymer (mg/L)</th>'
  //              + '<th scope="col">Forecasted Flow rate (cub. m/hr) </th></tr></thead><tbody>'
  //              + table_rows
  //              + '</tbody></table>';

  plot_ly_ts(response);
  // plot_graph(response,"polymer_concentration","Future Days", "Polymer Concentration(mg/L)", "Forecasted Polymer Concentration" );
  // plot_graph(response,"flow_rate","Future Days", "Flow rate (cub. m/hr)", "Forecasted Flow rate" );

  // $("#populated_result").append(table_string);

}

function populate_cost(response){

   $("#my_dataviz").empty('');
   $("#populated_result").empty('');
   table_rows = '';
   for (i = 0; i < response.predicted_cost.length; i++) {
    table_rows = table_rows + '<tr><th scope="row">' + (i+1).toString() + '</th><td>' + response.predicted_cost[i] + '</td><td></tr>';
   }
    table_string = '<table class="table table-striped"><thead><tr><th scope="col">Predicted Days' 
               + '</th><th scope="col">Forecasted Cost ($)</th>'
               + '</tr></thead><tbody>'
               + table_rows
               + '</tbody></table>';

  plot_graph(response,"cost","Future Days", "Cost ($)", "Forecasted Cost" );

  $("#populated_result").append(table_string);

}

function populate_turbidity(response){

   $("#my_dataviz").empty('');
   $("#populated_result").empty('');
   table_rows = '';
   for (i = 0; i < response.predicted_turbidity.length; i++) {
    table_rows = table_rows + '<tr><th scope="row">' + (i).toString() + '</th><td>' + response.predicted_turbidity[i] + '</td><td></tr>';
   }
    table_string = '<table class="table table-striped"><thead><tr><th scope="col">Predicted Days' 
               + '</th><th scope="col"> Forecasted Turbidity (NTU) </th>'
               + '</tr></thead><tbody>'
               + table_rows
               + '</tbody></table>';

  plot_graph(response,"turbidity","Future Days", "Turbidity (NTU)", "Forecasted Turbidity" );
  $("#populated_result").append(table_string);

}


function plot_graph(json_output, prediction_type, x_axis_value, y_axis_value, plot_title ){

  var data = []
  if (prediction_type == "cost") {
    for (i = 0; i < json_output.predicted_cost.length; i++) {
    data.push(
      {
        x:   i + 1,
        y:  json_output.predicted_cost[i]
      });
    }
  }

  if (prediction_type == "turbidity") {
    for (i = 0; i < json_output.predicted_turbidity.length; i++) {
    data.push(
      {
        x:   i ,
        y:  json_output.predicted_turbidity[i]
      });
    }

  }

  if (prediction_type == "fecl3_concentration") {
    for (i = 0; i < json_output.predicted_fecl3.length; i++) {
    data.push(
      {
        x:   i + 1,
        y:  json_output.predicted_fecl3[i]
      });
    }

  }

    if (prediction_type == "polymer_concentration") {
    for (i = 0; i < json_output.predicted_polymer.length; i++) {
    data.push(
      {
        x:   i + 1,
        y:  json_output.predicted_polymer[i]
      });
    }

  }

    if (prediction_type == "flow_rate") {
    for (i = 0; i < json_output.predicted_flow_rate.length; i++) {
    data.push(
      {
        x:   i + 1,
        y:  json_output.predicted_flow_rate[i]
      });
    }

  }


  var outerWidth  = 960, outerHeight = 500;    // includes margins

  var margin = {top: 100, right: 20, bottom: 80, left: 80};   // clockwise as in CSS

  var width = outerWidth - margin.left - margin.right,       // width of plot inside margins
    height = outerHeight - margin.top - margin.bottom;     // height   "     "

  document.body.style.margin="0px"; // Eliminate default margin from <body> element

  function xValue(d) { return d.x; }      // accessors
  function yValue(d) { return d.y; }

  var x = d3.scaleLinear()                // interpolator for X axis -- inner plot region
      .domain(d3.extent(data,xValue))
      .range([0,width]);

  var y = d3.scaleLinear()                // interpolator for Y axis -- inner plot region
      .domain(d3.extent(data,yValue))
      .range([height,0]);                  // remember, (0,0) is upper left -- this reverses "y"

  var line = d3.line()                     // SVG line generator
      .x(function(d) { return x(d.x); } )
      .y(function(d) { return y(d.y); } );

  var xAxis = d3.axisBottom(x)
      .ticks(5)                            // request 5 ticks on the x axis

  var yAxis = d3.axisLeft(y)                // y Axis
      .ticks(4)

  var svg = d3.select("#my_dataviz").append("svg")
      .attr("width",  outerWidth)
      .attr("height", outerHeight);        // Note: ok to leave this without units, implied "px"

  var g = svg.append("g")                  // <g> element is the inner plot area (i.e., inside the margins)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  g.append("g")                            // render the Y axis in the inner plot area
      .attr("class", "y axis")
      .call(yAxis);

  g.append("g")                            // render the X axis in the inner plot area
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")  // axis runs along lower part of graph
      .call(xAxis);

  g.append("text")                         // inner x-axis label
      .attr("class", "x label")
      .attr("text-anchor", "end")
      .attr("x", width - 6)
      .attr("y", height - 6)
      .text("");

  g.append("text")                         // outer x-axis label
      .attr("class", "x label")
      .attr("text-anchor", "end")
      .attr("x", width/2)
      .attr("y", height + 2*margin.bottom/3 + 6)
      .text(x_axis_value);

  g.append("text")                         // plot title
      .attr("class", "x label")
      .attr("text-anchor", "middle")
      .attr("x", width/2)
      .attr("y", -margin.top/2)
      .attr("dy", "+.75em")
      .text(plot_title);

  g.append("text")                         // inner y-axis label
      .attr("class", "y label")
      .attr("text-anchor", "end")
      .attr("x", -6)
      .attr("y", 6)
      .attr("dy", ".75em")
      .attr("transform", "rotate(-90)")
      .text("");

  g.append("text")                         // outer y-axis label
      .attr("class", "x label")
      .attr("text-anchor", "middle")
      .attr("x", -height/2)
      .attr("y", -6 - margin.left/3)
      .attr("dy", "-.75em")
      .attr("transform", "rotate(-90)")
      .text(y_axis_value);

  g.append("path")                         // plot the data as a line
      .datum(data)
      .attr("class", "line")
      .attr("d", line)
      .style('fill', 'none')
      .style('stroke', '#fff')
    .transition()
      .delay(500)
      .duration(1000)
      .style('stroke', '#000')

  g.selectAll(".dot")                      // plot a circle at each data location
      .data(data)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("cx", function(d) { return x(d.x); } )
      .attr("cy", function(d) { return y(d.y); } )
      .attr("r", 5);

}

