function plot_ts(x_data,y_data){
// var data = createRandomData(80,[0,1000],0.01);
// [{date:new Date('2013-01-01'),n:120,n3:200,ci_up:127,ci_down:115},...]
// xs = [1,2,3,4,5];
// ys = [2,4,6,8,10];
// ci_up_s = [3,5,7,9,11];
// ci_down_s = [1,3,5,7,9];
var data = [{n3:1000,n:2,ci_up_s:3000,ci_down_s:500},{n3:2000, n:4, ci_up_s:5000, ci_down_s: 1500},{n3:3000, n:6, ci_up_s:7000, ci_down_s: 2500},{n3:4000, n:8, ci_up_s:7000, ci_down_s: 3500},{n3:5000, n:10, ci_up_s:6000, ci_down_s: 4500}]



var chart = d3_timeseries()
  .addSerie(data.slice(0,5),{x:'n3',y:'n3'},{interpolate:'linear',color:"#a6cee3",label:"value"})
  .addSerie(data.slice(1),
          {x:'n3',y:'n3',ci_up:'ci_up_s',ci_down:'ci_down_s'},
          {interpolate:'monotone',dashed:true,color:"#a6cee3",label:"prediction"})
  .width(820)

chart('#chart')



// var dimensions = [ data.length, data[0].length ];

console.log(data);
console.log(typeof data)
// console.log(dimensions);
}







// var data = createRandomData(80,[0,1000],0.01)
// // [{date:new Date('2013-01-01'),n:120,n3:200,ci_up:127,ci_down:115},...]

// var chart = d3_timeseries()
//   .addSerie(data.slice(0,60),{x:'date',y:'n'},{interpolate:'linear',color:"#a6cee3",label:"value"})
//   .addSerie(data.slice(50),
//           {x:'date',y:'n3',ci_up:'ci_up',ci_down:'ci_down'},
//           {interpolate:'monotone',dashed:true,color:"#a6cee3",label:"prediction"})
//   .width(820)

// chart('#chart')