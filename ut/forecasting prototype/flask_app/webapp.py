import json
import os
import pandas as pd
import numpy as np


from flask import Flask, render_template, jsonify, request, redirect, url_for
from werkzeug.utils import secure_filename
from forecast_alg import *

application_root = os.path.abspath('.')
static_folder = os.path.join(application_root, 'static')
template_folder = os.path.join(application_root, 'templates')
app = Flask(__name__, static_url_path='/static', static_folder=static_folder, template_folder=template_folder)


@app.route('/')
def main():
   if request.args.get('radio_option') == None:
       radio_value = "disabled"
   else:
       radio_value = request.args.get('radio_option')
   if request.args.get('file_status') == None:
       file_status = "Upload"
   else:
       file_status = request.args.get('file_status')
   return render_template('index.html', radio_option=radio_value, file_status=file_status)


@app.route('/uploader', methods = ['POST'])
def uploader():
  if request.method == 'POST':
    f = request.files['file']
    filename = secure_filename(f.filename)
    f.save(os.path.join(static_folder, filename))
  return redirect(url_for('main', radio_option="enabled", file_status="File Uploaded!"))
    

@app.route('/api/predictor/<int:os>', methods = ['GET'])
def predictor(os):
    json_req, _ = forecastvalue(os)
    return jsonify(json_req)
   

@app.route('/api/dropdown', methods = ['GET'])
def droplist():
    _,title_list = forecastvalue()
    json_l = []
    for i in range(len(title_list)):
        temp = {"name": title_list[i],"abbreviation": i}
        json_l.append(temp)
    return jsonify(json_l)



def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

if __name__=="__main__":
    app.run(host='0.0.0.0', port=5003, debug=True)

