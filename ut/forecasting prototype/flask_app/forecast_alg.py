
import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval


def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test



def forecastvalue(os=0):    
    print("===================================", os)
    prop = os
    df = pd.read_csv('ut_export_alltime_copy.csv')
    
    df = df.drop(df.index[0:2])
    
    #Columns with a lot of NaNs
    cols = [7,13,26,27]
    df.drop(df.columns[cols],axis=1,inplace=True)
    
    print("dropping c_b,p_cfi, dp_cf, S_sum")
    
    df_req = df
    #Data Time columns
    cols = [0,1]
    df_req.drop(df_req.columns[cols],axis=1,inplace=True)
    
    # df_c1 = df_req.iloc[:,8]
    
    for col in df_req:
        df_req[col] = pd.to_numeric(df_req[col], errors='coerce')
    df_req = df_req.interpolate(method='linear', limit_direction='forward', axis=0)
    
    #Converting to arraw
    df_array = df_req.to_numpy()
    
    
    #Differencing to avoid seasonality
    diff_array = np.diff(df_array,axis=0)
    
    
    cut_off_range = 5000
    #Ignoring the outliers
    df_ar_no_out = diff_array[0:cut_off_range,:] 
    
    #Splitting test and train dataset
    data_train, data_test = split_data(df_ar_no_out)
    
    #Parameters
    forecast_days = 14
    order = 12
    #order 12 for prop 27, 17, 26, 27
    #order 12 not doing good for 8, 15
    
    model = VAR((data_train))
    
    model_fit = model.fit(order)
    
    ye = model_fit.forecast_interval(model_fit.y, steps = forecast_days)
    
    
    ye_for = ye[0]
    ye_for_l = ye[1]
    ye_for_u = ye[2]
    
    # prop = 26
    # plot_pacf(data_train[:,prop])
    
    
    dict_mse = []
    
    list_dict_mse = []
    
    for i in range(0,28):
        mse = mean_squared_error(ye_for[:,i], data_test[0:forecast_days,i])
        dict_mse.append({i:mse}) 
        list_dict_mse.append({order:dict_mse})
    
    
    
    
    #Differenced plots
    title_list = [
            "Flow Transmitter  [FT - 103] [m³/h]	",
            "FT for RO to PWST [FT - 104] [m³/h]	",
            "FT at ERT to RWST [FT - 105] [m³/h]	",
            "Temp Of CF Inlet [TT - 001] [°C]",	
            "Cond. at CF inlet [AIT -  003] [µS/cm]",	
            # "Cond. At ERT to RWST [AIT - 005] [µS/cm]",	
            "Cond. At Product water  [AIT - 006] [µS/cm]",   
            "PH of CF inlet [AIT - 001] [-]",    
            "ORP at CF inlet [AIT - 002] [mV]",  
            "PH of ERT To RWST [AIT - 013] [-]", 
            "PH of Product water [AIT - 007] [-]	",
            # "CF Inlet PT [kg/cm²]",
            "RO DPIT [DPIT - 101] [kg/cm²]", 
            "ERT outlet PT [PT - 005] [kg/cm²]", 
            "HPP outlet Pt [PT - 004] [kg/cm²]",  	
            "CF outlet PT [PT - 001] [kg/cm²]",	
            "HPP Load [A]",	 
            "HPP Power [kW]", 
            "Reject water tank level [LT - 005] [m]", 
            "Product water tank level [LT-  007] [m]",	
            "Totalizer RO A, B & C [m3]",
            "Totalizer Product outlet [FT - 108] [m³]",  
            "Totalizer Reject outlet [FT - 109] [m³]",	
            "Totalizer CF Inlet [FT - 111] [m³]",
            # "Differential Pressure [kg/cm²]",
            # "Conservation of Salt Flow [kg/h]",  
            "Conservation of Water Flow [m³/h]",    
            "A Average Flux [L/m²/h]",	
            "RO-A HPP Pressure Boost [kg/cm²]",
            "RO-A Membrane Recovery [%]",
            "A Salt Passage [%]",
            "A Salt Rejection [%]"
            ]
    
    
    title_list_without_units = [
            "Flow Transmitter ",
            "FT for RO to PWST ",
            "FT at ERT to RWST ",
            "Temp Of CF Inlet ",	
            "Cond. at CF inlet ",	
            # "Cond. At ERT to RWST [AIT - 005] [µS/cm]",	
            "Cond. At Product water ",   
            "PH of CF inlet ",    
            "ORP at CF inlet ",  
            "PH of ERT To RWST", 
            "PH of Product water ",
            # "CF Inlet PT [kg/cm²]",
            "RO DPIT ", 
            "ERT outlet PT ", 
            "HPP outlet Pt ",  	
            "CF outlet PT ",	
            "HPP Load",	 
            "HPP Power", 
            "Reject water tank level", 
            "Product water tank level ",	
            "Totalizer RO A, B & C ",
            "Totalizer Product outlet ",  
            "Totalizer Reject outlet ",	
            "Totalizer CF Inlet ",
            # "Differential Pressure [kg/cm²]",
            # "Conservation of Salt Flow [kg/h]",  
            "Conservation of Water Flow ",    
            "A Average Flux ",	
            "RO-A HPP Pressure Boost ",
            "RO-A Membrane Recovery ",
            "A Salt Passage ",
            "A Salt Rejection "
            ]
    
    
    #Inverting the difference
    initial_value = np.reshape(df_array[0,:],(1,-1))
    conc_data_train = np.concatenate((initial_value,data_train), axis=0)
    inv_data_train = np.cumsum(conc_data_train, axis = 0)
    
    
    init_forecast = np.reshape(inv_data_train[-1,:],(1,-1))
    conc_data_forecast = np.concatenate((init_forecast,ye_for), axis=0)
    conc_data_fore_ciu = np.concatenate((init_forecast,ye_for_u), axis=0)
    conc_data_fore_cil = np.concatenate((init_forecast,ye_for_l), axis=0)
    conc_data_test = np.concatenate((init_forecast,data_test), axis=0)
    
    
    inv_forecast = np.cumsum(conc_data_forecast, axis = 0)
    inv_forecast_u = np.cumsum(conc_data_fore_ciu, axis = 0)
    inv_forecast_l = np.cumsum(conc_data_fore_cil, axis = 0)
    inv_test_data = np.cumsum(conc_data_test, axis = 0)
    
    
    
    len_data_train = len(inv_data_train)
    x = np.arange(1,len_data_train+1)
    fore_x = np.arange(len_data_train+1,len_data_train+forecast_days+1)
    # fig, ax = plt.subplots()
    # ax.plot(fore_x,inv_forecast[1:,prop])
    # ax.fill_between(fore_x, inv_forecast_l[1:,prop], inv_forecast_u[1:,prop], color='b', alpha=0.1)
    # ax.plot(fore_x,inv_test_data[1:forecast_days+1,prop], color='k')
    # ax.plot(x[4810:],inv_data_train[4810:,prop], color='k')
    # # ax.xticks("Time steps")
    # # ax.yticks("Value")
    # ax.legend([" Forecasted value", "True value"])
    # # plt.savefig("demo_image.pdf")
    
    
    
    # fig, ax = plt.subplots()
    # ax.plot(fore_x, inv_forecast[1:,prop])
    # ax.plot(fore_x, inv_test_data[1:forecast_days+1,prop])
    # ax.fill_between(fore_x, inv_forecast_u[1:, prop], inv_forecast_l[1:, prop], alpha = 0.2)
    # ax.plot(x[4710:],inv_data_train[4710:,prop], color='k')
    
    diff_fore = ye_for[:,prop]
    diff_prev_x = x[4710:]
    diff_prev_y = data_train[4710:,prop]
    diff_test = data_test[0:forecast_days,prop]
    diff_fore_cil = ye_for_l[:,prop]
    diff_fore_ciu = ye_for_u[:,prop]
    
    
    
    fore_y = inv_forecast[1:,prop]
    fore_y_test = inv_test_data[1:forecast_days+1,prop]
    fore_y_ciu = inv_forecast_u[1:, prop]
    fore_y_cil = inv_forecast_l[1:, prop]
    prev_x_data = x[4710:]
    prev_y_data = inv_data_train[4710:,prop]
    title_plot = title_list[prop]
    
    # fore_xl = fore_x.to_list()
    # fore_yl = fore_y.to_list()
    # fore_y_testl = fore_y_test.to_list()
    # fore_y_ciul = fore_y_ciu.to_list()
    # fore_y_cill  = fore_y_cil.to_list()
    # print(title_plot)    
    test = np.arange(1,11)
    json_format = {"fore_x": fore_x.tolist(), "fore_y": fore_y.tolist(), "fore_y_test": fore_y_test.tolist(), "fore_y_ciu": fore_y_ciu.tolist(), "fore_y_cil" :fore_y_cil.tolist(), "prev_x_data": prev_x_data.tolist(), "prev_y_data": prev_y_data.tolist(),"title":title_plot, "diff_fore": diff_fore.tolist(), "diff_prev_x": diff_prev_x.tolist(), "diff_prev_y": diff_prev_y.tolist(), "diff_test": diff_test.tolist(), "diff_fore_cil": diff_fore_cil.tolist(), "diff_fore_ciu": diff_fore_ciu.tolist()  }
    # json_format = {"x":title_plot}
    return json_format, title_list
