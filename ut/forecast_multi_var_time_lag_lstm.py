
import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval


#sklearn and keras imports
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM


# convert series to supervised learning
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
	n_vars = 1 if type(data) is list else data.shape[1]
	df = DataFrame(data)
	cols, names = list(), list()
	# input sequence (t-n, ... t-1)
	for i in range(n_in, 0, -1):
		cols.append(df.shift(i))
		names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
	# forecast sequence (t, t+1, ... t+n)
	for i in range(0, n_out):
		cols.append(df.shift(-i))
		if i == 0:
			names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
		else:
			names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
	# put it all together
	agg = concat(cols, axis=1)
	agg.columns = names
	# drop rows with NaN values
	if dropnan:
		agg.dropna(inplace=True)
	return agg


def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

df = pd.read_csv('ut_export_alltime_copy.csv')

df = df.drop(df.index[0:2])

#Columns with a lot of NaNs
cols = [7,13,26,27]
df.drop(df.columns[cols],axis=1,inplace=True)

print("dropping c_b,p_cfi, dp_cf, S_sum")

df_req = df
#Data Time columns
cols = [0,1]
df_req.drop(df_req.columns[cols],axis=1,inplace=True)

# df_c1 = df_req.iloc[:,8]

for col in df_req:
    df_req[col] = pd.to_numeric(df_req[col], errors='coerce')
df_req = df_req.interpolate(method='linear', limit_direction='forward', axis=0)

#Converting to arraw
df_array = df_req.to_numpy()


#Differencing to avoid seasonality
diff_array = np.diff(df_array,axis=0)



#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 


#Scaling between 0 and 1
# normalize features
scaler = MinMaxScaler(feature_range=(0, 1))
scaled = scaler.fit_transform(df_ar_no_out)

# specify the number of lag hours
n_hours = 3
n_features = 8
# frame as supervised learning
reframed = series_to_supervised(scaled, n_hours, 1)
print(reframed.shape)


#Supervised Learning training set

cut_off_range = 5000


reframed_data = [cut_off_range]

#Splitting test and train dataset
data_train, data_test = split_data(reframed_data)



# plot_pacf(df_array[:,14],lags=70)

#Parameters
forecast_days = 14




# design network
model = Sequential()
model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
model.add(Dense(1))
model.compile(loss='mae', optimizer='adam')
# fit network
history = model.fit(train_X, train_y, epochs=50, batch_size=72, validation_data=(test_X, test_y), verbose=2, shuffle=False)
# plot history
pyplot.plot(history.history['loss'], label='train')
pyplot.plot(history.history['val_loss'], label='test')
pyplot.legend()
pyplot.show()



