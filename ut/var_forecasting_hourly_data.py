
import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

# import statsmodels.tsa.vector_ar.var_model.forecast_interval



def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test


df = pd.read_csv('ut_export_alltime_copy.csv')

df = df.drop(df.index[0:2])


#Columns with a lot of NaNs
cols = [7,13,26,27]
df.drop(df.columns[cols],axis=1,inplace=True)

print("dropping c_b,p_cfi, dp_cf, S_sum")

df_req = df
#Data Time columns
cols = [0,1]
df_req.drop(df_req.columns[cols],axis=1,inplace=True)

# df_c1 = df_req.iloc[:,8]

for col in df_req:
    df_req[col] = pd.to_numeric(df_req[col], errors='coerce')
df_req = df_req.interpolate(method='linear', limit_direction='forward', axis=0)

#Converting to arraw
df_array = df_req.to_numpy()


#Differencing to avoid seasonality
diff_array = np.diff(df_array,axis=0)

#Ignoring the outliers
df_ar_no_out = diff_array[0:6000,:] 

#Splitting test and train dataset
data_train, data_test = split_data(df_ar_no_out)


forecast_days = 14

model = VAR((data_train))
order = 33
model_fit = model.fit(order)
# make prediction
yhat = model_fit.forecast(model_fit.y, steps=forecast_days)
#Separating the desired forecasting
# chemical_pred_diff = yhat[:,0:3]    
# fecl3_pred =  inverse_diff(chemical_pred_diff[:,0],fecl3_ip[len(fecl3_ip)-forecast_days])
# polymer_pred =  inverse_diff(chemical_pred_diff[:,1],polymer_ip[len(polymer_ip)-forecast_days])
# in_flow_pred =  inverse_diff(chemical_pred_diff[:,2],in_flow_ip[len(in_flow_ip)-forecast_days])
# chemical_pred = [fecl3_pred, polymer_pred, in_flow_pred]
# chemical_pred, chemical_pred_diff


plt.plot(yhat[:,4])
plt.plot(data_test[0:14,4])


# plt.plot(df_ar_no_out[5796:5810:,2])
# plt.plot(data_test[0:14,2])
# plt.plot(df_array[0:7650,3])
# plt.close()




