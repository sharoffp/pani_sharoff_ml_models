#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# multivariate multi-step encoder-decoder lstm
from math import sqrt
from numpy import split
from numpy import array
from pandas import read_csv
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import RepeatVector
from keras.layers import TimeDistributed

import matplotlib.pyplot as plt

import numpy as np

import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
import numpy as np

from pandas.plotting import autocorrelation_plot
from pandas import DataFrame

#  partialcorrelation_plot and auto correlation plot
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf

#Models
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.arima_model import ARIMA

from sklearn.metrics import mean_squared_error
from math import sqrt

from statsmodels.tsa.vector_ar.var_model import forecast_interval

def split_data(data, ratio=0.966):    
    size = int(len(data) * ratio)
    train, test = data[0:size], data[size:len(data)]
    print("the train test split:",size,len(data)-size)
    return train, test

df = pd.read_csv('ut_export_alltime_copy.csv')

df = df.drop(df.index[0:2])

#Columns with a lot of NaNs
cols = [7,13,26,27]
df.drop(df.columns[cols],axis=1,inplace=True)

print("dropping c_b,p_cfi, dp_cf, S_sum")

df_req = df
#Data Time columns
cols = [0,1]
df_req.drop(df_req.columns[cols],axis=1,inplace=True)

# df_c1 = df_req.iloc[:,8]

for col in df_req:
    df_req[col] = pd.to_numeric(df_req[col], errors='coerce')
df_req = df_req.interpolate(method='linear', limit_direction='forward', axis=0)

#Converting to arraw
df_array = df_req.to_numpy()


#Differencing to avoid seasonality
diff_array = np.diff(df_array,axis=0)


cut_off_range = 5496
#Ignoring the outliers
df_ar_no_out = diff_array[0:cut_off_range,:] 

#Splitting test and train dataset
# data_train, data_test = split_data(df_ar_no_out)


# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[0:-120], data[-120:]
	# restructure into windows of weekly data
	print(np.shape(train), np.shape(test))
	train = array(split(train, len(train)/24))
	test = array(split(test, len(test)/24))
	return train, test

# evaluate one or more weekly forecasts against expected values
def evaluate_forecasts(actual, predicted):
	scores = list()
	# calculate an RMSE score for each day
	for i in range(actual.shape[1]):
		# calculate mse
		mse = mean_squared_error(actual[:, i], predicted[:, i])
		# calculate rmse
		rmse = sqrt(mse)
		# store
		scores.append(rmse)
	# calculate overall RMSE
	s = 0
	for row in range(actual.shape[0]):
		for col in range(actual.shape[1]):
			s += (actual[row, col] - predicted[row, col])**2
	score = sqrt(s / (actual.shape[0] * actual.shape[1]))
	return score, scores

# summarize scores
def summarize_scores(name, score, scores):
	s_scores = ', '.join(['%.1f' % s for s in scores])
	print('%s: [%.3f] %s' % (name, score, s_scores))

# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=24):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end <= len(data):
			X.append(data[in_start:in_end, :])
			y.append(data[in_end:out_end, 0])
		# move along one time step
		in_start += 1
	return array(X), array(y)

# train the model
def build_model(train, n_input):
	# prepare data
	train_x, train_y = to_supervised(train, n_input)
	# define parameters
	verbose, epochs, batch_size = 0, 50, 16
	n_timesteps, n_features, n_outputs = train_x.shape[1], train_x.shape[2], train_y.shape[1]
	# reshape output into [samples, timesteps, features]
	train_y = train_y.reshape((train_y.shape[0], train_y.shape[1], 1))
	# define model
	model = Sequential()
	model.add(LSTM(10, activation='relu', input_shape=(n_timesteps, n_features)))
	model.add(RepeatVector(n_outputs))
	model.add(LSTM(10, activation='relu', return_sequences=True))
	model.add(TimeDistributed(Dense(10, activation='relu')))
	model.add(TimeDistributed(Dense(1)))
	model.compile(loss='mse', optimizer='adam')
	# fit network
	model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose)
	return model

# make a forecast
def forecast(model, history, n_input):
	# flatten data
	data = array(history)
	data = data.reshape((data.shape[0]*data.shape[1], data.shape[2]))
	# retrieve last observations for input data
	input_x = data[-n_input:, :]
	# reshape into [1, n_input, n]
	input_x = input_x.reshape((1, input_x.shape[0], input_x.shape[1]))
	# forecast the next week
	yhat = model.predict(input_x, verbose=0)
	# we only want the vector forecast
	yhat = yhat[0]
	return yhat

# evaluate a single model
def evaluate_model(train, test, n_input):
	# fit model
	model = build_model(train, n_input)
	# history is a list of weekly data
	history = [x for x in train]
	# walk-forward validation over each week
	predictions = list()
	for i in range(len(test)):
		# predict the week
		yhat_sequence = forecast(model, history, n_input)
		# store the predictions
		predictions.append(yhat_sequence)
		# get real observation and add to history for predicting the next week
		history.append(test[i, :])
	# evaluate predictions days for each week
	predictions = array(predictions)
	score, scores = evaluate_forecasts(test[:, :, 0], predictions)
    # print(np.shape(predictions))
	return score, scores, predictions, test[:, :, 0]

# load the new file
# dataset = read_csv('household_power_consumption_days.csv', header=0, infer_datetime_format=True, parse_dates=['datetime'], index_col=['datetime'])
# split into train and test
train, test = split_dataset(df_ar_no_out)
# evaluate model and get scores
n_input = 24
score, scores, pred, test_pred = evaluate_model(train, test, n_input)
# summarize scores
summarize_scores('lstm', score, scores)
# plot scores
# days = ['sun', 'mon', 'tue', 'wed', 'thr', 'fri', 'sat']
# pyplot.plot(days, scores, marker='o', label='lstm')
# pyplot.show()


pred_reshape = pred.reshape(-1,24)

# plt.plot(pred)
# plt.plot(test_pred)
# plt.show()


for j in range(0,pred_reshape.shape[1]):
    plt.plot(pred_reshape[j,:])
    plt.plot(test_pred[j,:])
    plt.show()


# plt.plot
